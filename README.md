# Adaptive-Confidence BCMs

Grace Li and Jiajie Luo are the contributors to this repository. It is maintained by Grace Li.

This repository contains the source code and plots for our paper:

G. Li, J. Luo, M. A. Porter. "Bounded-Confidence Models of Opinion Dynamics with Adaptive Confidence Bounds." [arXiv preprint arXiv:2303.07563](https://arxiv.org/abs/2303.07563). March 2023.

We generalize Hegselman–Krause (HK) and Deffuant–Weisbuch (DW) bounded-confidence models (BCMs) of opinion dynamics to incorporate heterogeneous and adaptive confidence bounds. 
In our models, each pair of adjacent nodes has a distinct confidence bound which adpats over time depending on the nature of the interactions between those two nodes.

## Plots

To navigate to the plots we reference in the paper, find the folder corresponding to the model (DW or HK), network type, and network size (for synthetic networks) and open the "combined_plots" folder. For example, the plots for simulations of our adaptive-confidence HK model on 1000-node complete graphs can be found at `HK/Complete/complete1000/combined_plots`.

## Code

### Requirements

The code is implemented in Python 3.8.10. Package versions used for development are listed below.

```
python-igraph     0.9.7
numpy             1.21.3 
pandas            1.3.4
scipy             1.8.0 
matplotlib        3.4.3 
seaborn           0.12.0 
```

### Navigating the Code

We implement our adaptive-confidence DW and HK models in `AdaptiveConfidenceBCM.py`

For our adpative-confidence HK model, our simulation pipeline (in the `HK` folder) is as follows:
1. Our simulations are run using `syntheticGraphHK.py` and `facebookHK.py` for synthetic graphs and network data sets, respectively.
2. After running our simulations, we use `MatfileConsolidatorHK.py` to parse through the outputs and generate `.csv` files tabulating the results for each pair of confidence-growing and confidence-shrinking parameters $(\gamma, \delta)$. These files can be found in the `combined_results` folder of the appropriate network.
3. Using the tabulated outputs of `MatfileConsolidatorHK.py`, the various `linePlotsHK` files are used to generate the plots we present in our paper.
4. The script `effectiveGraphHK.py` generates visualizations of the final effective graph, a time-dependent subgraph of the original network with edges only between nodes that can influence each other at a given time.

For our adpative-confidence DW model, our simulation pipeline (in the `DW` folder) is analagous to that of our HK model, and is as follows:
1. Our simulations are run using `Complete/completeDW.py` and `NetworkDatasets/netscienceDW.py` for complete graphs and the NetScience network data set, respectively.
2. After running our simulations, we use `MatfileConsolidatorDW.py` to parse through the outputs and generate `.csv` files tabulating the results for each pair of confidence-growing and confidence-shrinking parameters $(\gamma, \delta)$. These files can be found in the `combined_results` folder of the appropriate network.
3. Using the tabulated outputs of `MatfileConsolidatorDW.py`, the various `linePlotsDW` files are used to generate the plots we present in our paper.
4. The script `effectiveGraphDW.py` generates visualizations of the final effective graph, a time-dependent subgraph of the original network with edges only between nodes that can influence each other at a given time.


## Citation

If you find this respository useful in your research, please consider citing [our paper](https://arxiv.org/abs/2303.07563).

G. Li, J. Luo, M. A. Porter. "Bounded-Confidence Models of Opinion Dynamics with Adaptive Confidence Bounds." arXiv preprint arXiv:2303.07563. March 2023.