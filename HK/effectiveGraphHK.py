'''
Author: Grace Li
Date: 9/10/2022

This script generates visualizations of the effective graph at convergence time for our adpative-confidence HK simulations.

For the graph visualizations, one can select whether to color nodes by initial or final opinion (using node_color_type) and 
whether to color edges black or by confidence bound (using edge_color_type). The include_cbar variable determines whether
the corresponding color bars are shown for the node/edge colors. 

For each simulation, this script outputs the visualization of the entire effective graph if it is not the same as the
original graph. If there are multiple opinion clusters, then each major opinion cluster is also visualized separately. 
The resulting visualizations are stored in the "effective_graphs" folder of the corresponding graph type/name.

NOTE: For our adaptive-confidence HK model simulations, the graphs with 1000 or more nodes are difficult to visualize 
completely. We recommend looking at individual opinion clusters instead (in the cases when consensus is not reached). 

IMPORTANT: This script requires igraph version 0.9.7. The syntax of that version is used for the visualizations.

'''
# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
# from matplotlib import animation
# import seaborn as sns
import time as time
import igraph as igraph
import os.path
# from os import getpid
from matplotlib.artist import Artist
from igraph import BoundingBox, Graph, palettes

# # Import our own adaptive-confidence bounded-confidence module
# import sys
# sys.path.append('..') #look one directory above
# import AdaptiveConfidenceBCM

minor_clusters_percent = 1

# #name of experiment folder
graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 1000

graph_name = "Reed" #If we have a specific graph
facebook_dataset = False #Set equal to true if we want to use graph name
if facebook_dataset:
    graph_type = "facebook"

#whether to color nodes by initial or final opinion
node_color_type = "initial_opinion"

#whether to color edges black or by confidence bound
edge_color_type = "black"

#Whether or not to include a colorbar
include_cbar = True

#Whether or not to include information in the title
show_title = True

if graph_type == "erdos-renyi":
    p = 0.1 #edge probability for ER graph
    
#SBM edge probabilities
if graph_type == "SBM":
    p_aa, p_bb = 1, 1
    p_ab = 0.01
    pref_matrix = [[p_aa, p_ab], [p_ab, p_bb]]

gammas = [0.1, 0.01, 0.05, 0.005, 0.001, 0.0005, 0.0001] #Confidence-increase parameter
deltas = [0.01, 0.1, 0.5, 0.9, 0.95, 0.99] #Confidence-decrease parameter

gamma_delta_pairs = [(0.001, 0.5)] #, (0.005, 0.5)]

#Initial confidence bound
cs = [0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1]

opinion_sets = list(range(10))

#Specify which graphs to look at if using a random-graph model
graph_numbers = [1] #list(range(0,5))
if facebook_dataset or graph_type == "complete":
    graph_numbers = [0] #there is only one graph to look at

cs = [0.1]
opinion_sets = [4]

#savefolder name for experiment
folder_names = {'complete': 'Complete', 'erdos-renyi': 'Erdos-Renyi', "SBM":"SBM"}
random_graph_types = ['erdos-renyi', 'SBM']

if facebook_dataset:
    experiment = 'Facebook100/' + graph_name
else:
    experiment = f"{folder_names[graph_type]}/{graph_type}{n}"

fontsizes = {'S':22, 'M':30, 'L':30}
    
## ------------------------------------------------------

# Make Matplotlib use a Cairo backend
mpl.use("cairo")

#colorbar colormaps
opinion_cmap = plt.cm.rainbow
opinion_norm = mpl.colors.Normalize(vmin=0, vmax=1)
cmap = plt.cm.inferno_r #rainbow_r

#Class to handle the igraph plot and generate it as desired
class GraphArtist(Artist):
    """Matplotlib artist class that draws igraph graphs.
    Only Cairo-based backends are supported.
    """

    def __init__(self, graph, bbox, palette=None, *args, **kwds):
        """Constructs a graph artist that draws the given graph within
        the given bounding box.

        `graph` must be an instance of `igraph.Graph`.
        `bbox` must either be an instance of `igraph.drawing.BoundingBox`
        or a 4-tuple (`left`, `top`, `width`, `height`). The tuple
        will be passed on to the constructor of `BoundingBox`.
        `palette` is an igraph palette that is used to transform
        numeric color IDs to RGB values. If `None`, a default grayscale
        palette is used from igraph.

        All the remaining positional and keyword arguments are passed
        on intact to `igraph.Graph.__plot__`.
        """
        Artist.__init__(self)

        if not isinstance(graph, Graph):
            raise TypeError("expected igraph.Graph, got %r" % type(graph))

        self.graph = graph
        self.palette = palette or palettes["gray"]
        self.bbox = BoundingBox(bbox)
        self.args = args
        self.kwds = kwds

    def draw(self, renderer):
        from matplotlib.backends.backend_cairo import RendererCairo
        if not isinstance(renderer, RendererCairo):
            raise TypeError("graph plotting is supported only on Cairo backends")
        self.graph.__plot__(renderer.gc.ctx, self.bbox, self.palette, *self.args, **self.kwds)

## ------------------------------------------------------

#Read the graph if it's a facebook data set
if facebook_dataset:
    graph_matfile = f"Facebook100/network_data/{graph_name}.mat"
    data = io.loadmat(graph_matfile)
    G = igraph.Graph.Adjacency(data['A'].toarray(), mode = "undirected")
    print(graph_name)
    print('Original number of vertices:', G.vcount())
    del data

    component_membership = G.clusters(mode='strong').membership
    sizes = G.clusters(mode='strong').sizes()
    index_max = sizes.index(max(sizes))
    component = np.nonzero(np.array(component_membership)==index_max)[0]
    G_original = G.induced_subgraph(component)
    n = G.vcount()
    print('GCC number of vertices:', n)

#Initialize complete graph
elif graph_type == "complete":
    G_original = igraph.Graph.Full(n)
    
# Get the random graph seed if its a random graph
elif graph_type in random_graph_types:
    graph_seed_file = folder_names[graph_type] + f"/{graph_type}{n}/graph_seeds.csv"
    df = pd.read_csv(graph_seed_file)
    if graph_type == 'erdos-renyi':
        df = df[df['p'] == p]
    elif graph_type == "SBM":
        df = df[df['p_aa'] == pref_matrix[0][0]]
        df = df[df['p_ab'] == pref_matrix[0][1]]
        df = df[df['p_bb'] == pref_matrix[1][1]]  
    graph_seed = df['graph_seed'].values[0]
    graph_seed = int(graph_seed)

#Loop through the parameters and generate the effective subgraph plot
for graph_number in graph_numbers:

    if graph_type in random_graph_types: 
        #Reinitialize the random seed and generate the corresponding graph number from that seed
        if graph_type == "erdos-renyi":
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G_original = igraph.Graph.Erdos_Renyi(n, p)
        elif graph_type == "SBM":
            random_graph = np.random.default_rng(graph_seed)
            #Block sizes
            A = int(n * 0.75)
            B = int(n * 0.25)
            block_sizes = [A, B]
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G_original = igraph.Graph.SBM(n, pref_matrix, block_sizes, directed=False, loops=False)
    
    for pair in gamma_delta_pairs:
        gamma, delta = pair[0], pair[1]

        #make sure that the plot folder exists
        directory = experiment + "/effective_graphs/"
        if graph_type in random_graph_types:
            if graph_type == "erdos-renyi":
                directory = directory + f"p{p}/"
            elif graph_type == "SBM":
                directory = directory + f"p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}/"
        directory = directory + f"delta{delta}-gamma{gamma}"
        if not os.path.exists(directory):
            os.makedirs(directory)

        for c in cs:
            for opinion_set in opinion_sets:

                #Load the matfile for this experiment
                if graph_type == "erdos-renyi" and not facebook_dataset:
                    matfile = (
                                f"{experiment}/matfiles/p{p}"
                                f"/graph{graph_number}/delta{delta}-gamma{gamma}"
                                f"/p{p}-graph{graph_number}--delta{delta}-gamma{gamma}"
                                f"--c{c}-op{opinion_set}.mat"
                               )
                elif graph_type == "SBM" and not facebook_dataset:
                    matfile = (
                                f"{experiment}/matfiles/p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}"
                                f"/graph{graph_number}/delta{delta}-gamma{gamma}"
                                f"/graph{graph_number}--delta{delta}-gamma{gamma}"
                                f"--c{c}-op{opinion_set}.mat"
                              )
                else:
                    matfile = (
                                 f"{experiment}/matfiles/delta{delta}-gamma{gamma}"
                                 f"/delta{delta}-gamma{gamma}--c{c}-op{opinion_set}.mat"
                               )
                try:
                    results = io.loadmat(matfile)
                    file_found = True
                except:
                    file_found = False

                if file_found:
                    #Reinitialize graph
                    G = G_original.copy()

                    G.vs['opinion'] = results['final_opinions'][0] #assign final opinions
                    avg_opinion = sum(G.vs['opinion']) / len(G.vs['opinion'])

                    ## Get the edge information for the graph
                    # Store the start and end nodes for each edge (igraph will return edge_start < edge_end for each edge)
                    # and the magnitude of the opinion difference and confidence bound on that edge as numpy arrays
                    # Edge start and end
                    edge_start = np.array([e.tuple[0] for e in G.es], dtype=np.int64)
                    edge_end = np.array([e.tuple[1] for e in G.es], dtype=np.int64)
                    n_edges = len(edge_start)

                    # Magnitude of the opinion difference across each edge
                    opinion_diff = np.array(G.vs[edge_start.tolist()]['opinion']) - np.array(G.vs[edge_end.tolist()]['opinion'])
                    opinion_diff = np.abs(opinion_diff)

                    #Get the confidence bound for each edge
                    confidence = results['confidence'][0]

                    #Get the edges which have opinion difference < confidence (the confidence bound) and convert them to an edge list
                    index = np.argwhere(opinion_diff < confidence)
                    index = index.flatten()
                    sub_edge_start = edge_start[index]
                    sub_edge_end = edge_end[index]
                    edge_list = [(sub_edge_start[i], sub_edge_end[i]) for i in range(len(index))]

                    #Construct an effective subgraph subG consisting of these edges with possible interactions 
                    subG = igraph.Graph()
                    subG.add_vertices(n)
                    subG.add_edges(edge_list)
                    subG.vs['opinion'] = G.vs['opinion']
                    subG.es['confidence'] = confidence[index]

                    #Assign the initial opinion values
                    subG.vs['init_opinion'] = results['init_opinions'][0]

                    #Get the opinion clusters, the components of the subgraph
                    component_membership = subG.clusters(mode='strong').membership
                    n_components = max(component_membership) + 1
                    components = []
                    for i in range(n_components):
                        component = np.nonzero(np.array(component_membership)==i)[0]
                        components.append(component.tolist())

                    #Calculate the Shannon entropy for the normalized clusters if n_clusters is not zero from hitting the bailout
                    n_clusters = results['n_clusters'][0][0]
                    #If we sucessfully determined the clusters, then we calculate the Shannon entropy and max cluster diameter
                    if n_clusters > 0:
                        cluster_sizes = [] #get and store the size of each opinion cluster
                        for i in range(n_clusters):
                            key = "cluster" + str(i)
                            cluster = results[key][0]
                            cluster_sizes.append(len(cluster))

                            final_ops = results['final_opinions'][0][cluster]
                        cluster_sizes = np.array(cluster_sizes) / np.sum(np.array(cluster_sizes)) #normalize the cluster sizes
                        #Calculate Shannon's entropy of normalized clusters
                        H = - np.sum(cluster_sizes * np.log(cluster_sizes))

                        #Fraction of edges in the subgraph
                        frac_edges =  len(subG.es) / n_edges

                    #If we don't have all the edges in the original graph, plot the effective subgraph:
                    if frac_edges < 1:
                        # Create the figure
                        fig, ax = plt.subplots(figsize = (11,10))

                        #Get the norm of the edge confidence colorbar with some rounding
                        vmin = math.floor(10 * min(subG.es['confidence'])) / 10
                        vmax = math.ceil(10 * max(subG.es['confidence'])) / 10
                        if vmax > 1.0: #cap the max confidence bound at 1
                            vmax = 1.0
                        if vmin < 0.0: #cap the min confidence bound at 0 
                            vmin = 0.0
                        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

                        if node_color_type == "initial_opinion":
                            vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in subG.vs['init_opinion']]
                        elif node_color_type == "final_opinion":
                            vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in subG.vs['opinion']]

                        if edge_color_type == "confidence":
                            edge_color = [cmap(norm(confidence)) for confidence in subG.es['confidence']]
                        else:
                            edge_color = "black"

                        visual_style = {
                                        'vertex_size' : 5,
                                        'vertex_frame_width' : 0.5,
                                        'vertex_frame_color' : 'black',
                                        'vertex_color' : vertex_color,
                                        'edge_width' : 1,
                                        'edge_color' : edge_color,
                                        'layout': subG.layout_fruchterman_reingold() #layout_lgl()
                                        }

                        # Draw the graph over the plot
                        # Two points to note here:
                        # 1) we add the graph to the axes, not to the figure. This is because
                        #    the axes are always drawn on top of everything in a matplotlib
                        #    figure, and we want the graph to be on top of the axes.
                        # 2) we set the z-order of the graph to infinity to ensure that it is
                        #    drawn above all the curves drawn by the axes object itself.
                        #(`left`, `top`, `width`, `height`)
                        if include_cbar and show_title:
                            graph_artist = GraphArtist(subG, (60, 300, 700, 800), **visual_style)
                        elif include_cbar and not show_title:
                            graph_artist = GraphArtist(subG, (60, 100, 600, 700), **visual_style)
                        else:
                            graph_artist = GraphArtist(subG, (150, 300, 800, 700), **visual_style)
                        # graph_artist.set_zorder(float('inf'))
                        ax.artists.append(graph_artist)

                        plt.axis('off') #hide axes

                        if include_cbar:
                            #Colorbar for nodes colored by initial opinion
                            if node_color_type == "initial_opinion":
                                cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=opinion_norm, cmap=opinion_cmap), ax=ax, shrink = 0.9)
                                cbar.ax.tick_params(labelsize=fontsizes['S'])
                                cbar.set_label('Initial opinion', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                            #Add the colorbar if the confidence bound is changing
                            else:
                                if not (delta == 1.0 and gamma == 0.0):
                                    cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, shrink = 0.9)
                                    cbar.ax.tick_params(labelsize=fontsizes['S'])
                                    cbar.set_label('Confidence bound', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                        #Add title
                        if show_title:
                            title = "Final Effective Graph\n"
                            title = (
                                        title + r"$\gamma$=" + str(gamma) + ", $\delta$=" + str(delta)
                                        + r", $c_0$=" + str(c) + ', op=' + str(opinion_set)
                                        + '\nFraction of edges: ' + "{0:.6g}".format(frac_edges)
                                        + '\nMean opinion: ' + str(round(avg_opinion, 3)) 
                                    )
                            if n_clusters > 0 or results['bailout'][0][0]:
                                if n_clusters > 0:
                                    title = title + ', Entropy: ' + str(round(H, 3))
                                # if results['bailout'][0][0]:
                                #     title = title + ' with bailout time reached'
                            ax.set_title(title, fontsize=fontsizes['L'])
                        else:
                            ax.set_title(" ", fontsize=fontsizes['L'])

                        #Save the file
                        savefile = directory + "/"
                        if graph_type in random_graph_types:
                            savefile = savefile + f"graph{graph_number}--"
                        savefile = savefile + f"delta{delta}-gamma{gamma}--c{c}-op{opinion_set}"
                        if not show_title:
                            savefile = savefile + "--no_title"
                        savefile = savefile + ".png"
                        print(savefile)
                        plt.savefig(savefile, facecolor="white", bbox_inches='tight')
                        plt.close()


                        #Generate a new plot for each major opinion cluster
                        if n_components > 1:
                            for i in range(n_components):
                                if len(components[i]) > minor_clusters_percent/100 * n: 

                                    cluster = subG.induced_subgraph(components[i])

                                    #Fraction of edges in the subgraph
                                    cluster_original = G.induced_subgraph(components[i])
                                    frac_edges =  len(cluster.es) / len(cluster_original.es)

                                    #Only plot the components that are not complete to check them out
                                    if frac_edges < 1:

                                        try:
                                            vmin = math.floor(10 * min(cluster.es['confidence'])) / 10
                                            vmax = math.ceil(10 * max(cluster.es['confidence'])) / 10
                                            if vmax > 1.0: #cap the max confidence bound at 1
                                                vmax = 1.0
                                            if vmin < 0.0: #cap the min confidence bound at 0 
                                                vmin = 0.0
                                        except:
                                            vmin = 0.0
                                            vmax = 1.0
                                        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

                                        if node_color_type == "initial_opinion":
                                            vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in cluster.vs['init_opinion']]
                                        elif node_color_type == "final_opinion":
                                            vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in cluster.vs['opinion']]

                                        if edge_color_type == "confidence":
                                            edge_color = [cmap(norm(confidence)) for confidence in subG.es['confidence']]
                                        else:
                                            edge_color = "black"

                                        visual_style = {
                                                'vertex_size' : 10,
                                                'vertex_frame_width' : 1,
                                                'vertex_frame_color' : 'black',
                                                'vertex_color' : vertex_color,
                                                'edge_width' : 1,
                                                'edge_color' : edge_color,
                                                'layout': cluster.layout_lgl() #layout_kamada_kawai() #layout_fruchterman_reingold()
                                                }

                                        # Create the figure
                                        fig, ax = plt.subplots(figsize = (12,10))

                                        # Draw the graph over the plot
                                        # Two points to note here:
                                        # 1) we add the graph to the axes, not to the figure. This is because
                                        #    the axes are always drawn on top of everything in a matplotlib
                                        #    figure, and we want the graph to be on top of the axes.
                                        # 2) we set the z-order of the graph to infinity to ensure that it is
                                        #    drawn above all the curves drawn by the axes object itself.
                                        #(`left`, `top`, `width`, `height`)
                                        if include_cbar and show_title:
                                            graph_artist = GraphArtist(cluster, (60, 200, 700, 800), **visual_style)
                                        elif include_cbar and not show_title:
                                            graph_artist = GraphArtist(cluster, (60, 100, 700, 750), **visual_style)
                                        else:
                                            graph_artist = GraphArtist(cluster, (150, 200, 800, 800), **visual_style)
                                        # graph_artist.set_zorder(float('inf'))
                                        ax.artists.append(graph_artist)

                                        plt.axis('off') #hide axes

                                        if include_cbar:
                                            #Colorbar for nodes colored by initial opinion
                                            if node_color_type == "initial_opinion":
                                                cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=opinion_norm, cmap=opinion_cmap), ax=ax, shrink = 0.9)
                                                cbar.ax.tick_params(labelsize=fontsizes['S'])
                                                cbar.set_label('Initial opinion', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                                            #Add the colorbar if the confidence bound is changing
                                            else:
                                                if not (delta == 1.0 and gamma == 0.0):
                                                    cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, shrink = 0.9)
                                                    cbar.ax.tick_params(labelsize=fontsizes['S'])
                                                    cbar.set_label('Confidence bound', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                                        #Get the averge opinion and diameter of the cluster
                                        avg_opinion = sum(cluster.vs['opinion']) / len(cluster.vs['opinion'])
                                        diameter = max(cluster.vs['opinion']) - min(cluster.vs['opinion'])

                                        # title = "Cluster " + str(i) + " Effective Subgraph\n"
                                        # title = (
                                        #             title + r"$\delta$=" + str(delta) + r", $\gamma$=" + str(gamma) 
                                        #             + r", $c$=" + str(c) + ', op=' + str(opinion_set)
                                        #             + '\n' + 'Mean opinion: ' + str(round(avg_opinion, 3)) 
                                        #             + ', Cluster diameter: ' + "{0:.4e}".format(diameter)
                                        #             + '\n' + 'Fraction of edges: ' + "{0:.6g}".format(frac_edges)
                                        #          )
                                        # ax.set_title(title,fontsize=fontsizes['L'])

                                        #Add title
                                        if show_title:
                                            # title = "Final Effective Subgraph\n"
                                            title = "Adaptive-Confidence HK Model\n"
                                            title = title + r"Example Final Opinion Cluster in $\mathit{G_{eff}(T)}$" + "\n" #" for Complete(1000) \n"
                                            title = (title + r"$\mathit{\gamma}=$" + str(gamma) +  r"$, \mathit{\delta}=$" + str(delta) + r", $\mathit{c_0}=$" + str(c))
                                            ax.set_title(title, fontsize=fontsizes['L'])
                                        else:
                                            ax.set_title(" ", fontsize=fontsizes['L'])

                                        #Save the file
                                        savefile = directory + "/"
                                        if graph_type in random_graph_types:
                                            savefile = savefile + f"graph{graph_number}--"
                                        savefile = savefile + f"delta{delta}-gamma{gamma}--c{c}-op{opinion_set}--cluster{i}"
                                        if not show_title:
                                            savefile = savefile + "--no_title"
                                        savefile = savefile + ".png"
                                        print(savefile)
                                        plt.savefig(savefile, facecolor="white", bbox_inches='tight')
                                        plt.close()
