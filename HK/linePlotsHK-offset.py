'''
Author: Grace Li
Date: 11/1/2022

This script generates plots of the results of the simulations for our adaptive-confidence HK BCM.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, W(T), and convergence time. 
Here, we also plot the variance of final opinions, mean local receptiveness, fraction of edges in the effective graph. 
and the fraction of deleted edges overall in the effective graph and within clusters of the effective graph.
For each quantity of interest, this script generates a single figure where each plot shows the quantity of interest
versus the initial confidence bound c0 (called c in the code). Each plot represents a single confidence-increase 
parameter gamma value. Each plot has different colored curves corresponding to different confidence-decrease delta values. 
We plot the mean with error range representing one standard deviation. For fixed graphs (i.e., complete and Facebook100 graphs), 
we generate each point on the plot from 10 numerical simulations each with different sets of initial opinions drawn uniformly at random. 
For random graphs (i.e., Erdos-Renyi and SBM graphs), we generate each point on the plot from 50 numerical simulations 
from 5 randomly drawn graphs each with 10 sets of initial opinions drawn uniformly at random.


We used this script to generate our figures for our simulations of our adaptive-confidence HK model on SBM random graphs
and the Facebook100 networks. It generates figures which have 5 plots. Since they look small when put in the same row, 
the plots are offset, with the baseline model on the left, and a grid of 4 plots on the right. 

'''

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# # Import our own adaptive-confidence bounded-confidence module
# import sys
# sys.path.append('..') #look one directory above
# import AdaptiveConfidenceBCM

sns.set_style("ticks",{'axes.grid' : True})

#Change experiment parameters here -----------------------------------------------

#Options are: “se” for standard error or "sd" for standard deviation
error_type = "sd"

#Whether to look at the smaller/more interesting plot, or plot to the maximum c value for the x-axis
plot_max_c = False

#Whether or not to print a plot title
show_plot_title = False

#Graph type
facebook_networks = ["Reed", "Swarthmore", "Oberlin", "Pepperdine", "Rice", "UCSB"] #options for Facebook100 networks
network_dataset = True #Set equal to true if we want to use a particular graph (e.g., one of the Facebook100 networks or the NetScience network)
graph_name = "UCSB" #If we have a specific graph

#Graph type for synthetic graphs. Options are: "complete", "erdos-renyi", "SBM"
graph_type = "SBM"
n = 1000

if graph_type == "erdos-renyi":
    p = 0.1

# Change SBM parameters here:
#Edge probabilities
if graph_type == "SBM":
    p_aa, p_bb = 1, 1
    p_ab = 0.01
    pref_matrix = [[p_aa, p_ab], [p_ab, p_bb]]

runtoTmax = False
toltest = False
tol = 0.01 #specify the tolerance value if its not the standard

#Specify which quantities to plot
keys = [
        'log(T)', 'entropy',
        'n_major', 'n_minor',
        'wt_avg_frac_edges_cluster'
       ]
# keys = [
#         'log(T)',
#         'n_clusters', 'entropy',
#         'avg_local_receptiveness',
#         'n_major', 'n_minor',
#         'op_var',
#         'frac_edges', 'wt_avg_frac_edges_cluster',
#         'deleted_within_of_total', 'deleted_within_of_deleted'
#        ]

#Confidence-increase parameter
gammas = [0.001, 0.005, 0.01, 0.05]

#Confidence-decrease parameter
deltas = [0.5, 0.9, 0.95, 0.99, 1.0]
    
#seaborn_colorblind = ['#0173b2', '#de8f05', '#029e73', '#d55e00', '#cc78bc', '#ca9161', '#fbafe4', '#949494', '#ece133', '#56b4e9']
colorlist = ['#0173b2', '#de8f05', '#029e73', '#d55e00', '#56b4e9']
markerlist = ["o", "X", "s", "^", "*"]

#Initial confidence bound
cs = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.3, 0.4, 0.5]

min_c = 0.018
max_c = 0.202
custom_xticks = True
xtick_list = [0.02, 0.05, 0.10, 0.15, 0.20]
drop_cs = [0.01] #, 0.3, 0.4, 0.5]
if plot_max_c:
    max_c = max(cs) + 0.002
    drop_cs = [0.01]
    xtick_list = [0.05, 0.10, 0.15, 0.20, 0.3, 0.4, 0.5]
        
#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi", "SBM":"SBM"}
random_graph_folders = ["Erdos-Renyi", "SBM"] #The names of the folders with random-graph models
        
#Plot parameters  ---------------------------------------------------------------------------
#Plot font size parameters
fontsizes = {'XS': 13, 'S': 16, 'M': 24, 'L':25, 'XL': 35, 'XXL':45, 'space':30, 'left_space': 85} 

# color_palette = ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33','#a65628']
# color_palette = sns.color_palette("viridis", as_cmap=True)

plot_height = 5
if show_plot_title:
    plot_height = 5.7
plot_width = 5.4
bbox_to_anchor=(1.0, 0.55)
rows, cols = 2, 3

plot_name = {
                'log(T)': 'T',
                'n_clusters': 'clusters', 
                'n_minor': 'n_minor',
                'n_major': 'n_major',
                'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis',
                'frac_edges': 'frac_edges', 
                'wt_avg_frac_edges_cluster': 'wt_avg_frac_edges',
                'deleted_within_of_total': 'deleted_within_of_total',
                'deleted_within_of_deleted': 'deleted_within_of_deleted'
            }
plot_ylabel = {
                'log(T)': r"$\log_{10}(T_f)$", 
                'n_clusters': 'Number of clusters',
                'n_minor': 'Number of minor clusters',
                'n_major': 'Number of major clusters',
                'entropy': 'Shannon entropy ' + r"$H(T_f)$",
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis',
                'frac_edges': 'Fraction of edges', 
                'wt_avg_frac_edges_cluster': r"$W(T_f)$",
                'deleted_within_of_total': 'Fraction of\ndisconnected edges',
                'deleted_within_of_deleted': 'Fraction of within-cluster\ndisconnected edges'
             }

plot_title = plot_ylabel.copy()
plot_title['deleted_within_of_total'] = 'Fraction of disconnected edges'
plot_title['deleted_within_of_deleted'] = 'Fraction of within-cluster disconnected edges'


#Names for the columns of the consolidated matfiles
columns = ['delta', 'gamma', 'c', 'opinion_set',
           'n_clusters', 'n_major', 'n_minor',
           'max_diameter', 'T', 'bailout', 
           'avg_opinion_diff', 'avg_local_receptiveness', 'entropy',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis',
           'frac_edges', 'wt_avg_frac_edges_cluster',
           'deleted_within_of_total', 'deleted_within_of_deleted'
          ]

idx_to_letter = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 8:'I', 9:'J', 10:'K'}

# Plot results for each gamma/delta pair ---------------------------------------------------

# for graph_name in facebook_networks:
#     print(graph_name)

#Name of experiment folder
if network_dataset:
    graph_type = graph_name
    folder_name = f"Facebook100/{graph_name}"
    if graph_name == "netscience":
        folder_name = "NetScience"
    ns = [1]

    experiment = 'Facebook100/' + graph_name + ("/runtoTmax" * runtoTmax)
    if graph_name == "netscience":
        experiment = 'NetScience' + ("/runtoTmax" * runtoTmax)

else:
    folder_name = folder_name_dict[graph_type]
    experiment = folder_name_dict[graph_type] + '/' + graph_type + str(n) + ("/runtoTmax" * runtoTmax)

#Check that a directory exists, and if not, create it
directory = experiment + f"/combined_plots"
if not network_dataset and graph_type == "erdos-renyi":
    directory = directory + f"/p{p}"
if error_type == "se":
    directory = experiment + f"/combined_plots/plot_grid_{error_type}"
if plot_max_c:
    directory = directory + "/plot_max_c"
if not os.path.exists(directory):
    os.makedirs(directory)

#Loop through the quantities of interest
for key in keys:
    print(key)

    #index to label each plot panel
    label_idx = 0

    fig = plt.figure(figsize=(plot_width*cols, plot_height*rows))
    gs = mpl.gridspec.GridSpec(rows*2, cols)

    #Generate each subplot

    ## Plot the baseline model
    ax = plt.subplot(gs[1:3, 0])

    delta, gamma = 1.0, 0.0

    #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
    filename = experiment + '/combined_results'
    if not network_dataset:
        if graph_type == "erdos-renyi":
            filename = filename + f"/p{p}"
        if graph_type == "SBM":
            filename = filename + f"/p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}"
    filename = filename + '/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
    sim_results = pd.read_csv(filename)
    # print(filename)
    # print(sim_results.columns)

    #Make sure the opinion_set, and time steps are integers instead of floats
    sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'n_clusters': float})

    #Calculate the log of time steps to make it easier to visualize
    sim_results['log(T)'] = np.log10(sim_results['T'])

    #Drop the rows with c values we don't want to plot
    sim_results = sim_results[~sim_results['c'].isin(drop_cs)]

    #Generate pointplot for visualization for this delta value
    # sns.pointplot(x='c', y=key, ax = ax, data=sim_results)
    sns.lineplot(x='c', y=key, ax = ax, data=sim_results, 
                 hue = "delta", palette = ["black"], style = "delta", 
                 markers = ["."], markersize=10, legend=False)

    title = "Baseline HK model\n" + r"($\gamma = 0$, $\delta = 1$)"
    ax.set_title(title, fontsize = fontsizes['L'], pad = fontsizes['XS'])

    # ax.set_xlabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['space'])
    # ax.xaxis.label.set_color('white')
    ax.set(xlabel=None)
    ax.set_ylabel(plot_ylabel[key], fontsize = fontsizes['XXL'], labelpad = fontsizes["left_space"], va = "center")
    # ax.set_ylabel(plot_title[key], fontsize = fontsizes['space'])
    # ax.yaxis.label.set_color('white')
    # ax.set(ylabel=None)
    ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
    ax.set_xlim(min_c, max_c)
    if plot_max_c:
        ax.tick_params(axis = 'x', rotation = 45)
    if custom_xticks:
        ax.set_xticks(xtick_list)
    if key in ['frac_edges', 'wt_avg_frac_edges_cluster', 'deleted_within_of_total', 'deleted_within_of_deleted']:
        ax.set_ylim(-0.05, 1.05)
        ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

    ax.text(-0.08, 1.2, idx_to_letter[label_idx], transform=ax.transAxes, fontsize=fontsizes['XL'], fontweight='bold', va='top', ha='right')

    #Plot the subplots for gamma > 0
    for row in range(rows):
        for col in range(1, cols):

            label_idx +=1

            ax = plt.subplot(gs[2*row:2*row + 2, col])

            #Get the gamma value for this plot
            index = row*(cols-1) + (col-1)
            gamma = gammas[index]

            #Initialize the dataframe to concatenate results for all the delta values for this particular gamma
            df = pd.DataFrame(columns = columns)

            for delta in deltas:

                #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
                filename = experiment + '/combined_results'
                if not network_dataset:
                    if graph_type == "erdos-renyi":
                        filename = filename + f"/p{p}"
                    if graph_type == "SBM":
                        filename = filename + f"/p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}"
                filename = filename + '/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
                sim_results = pd.read_csv(filename)

                #Make sure the opinion_set, and time steps are integers instead of floats
                sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'n_clusters': float})

                #Calculate the log of time steps to make it easier to visualize
                sim_results['log(T)'] = np.log10(sim_results['T'])

                #Convert delta to a string so the legend displays properly
                sim_results['delta'] = str(delta)
                if delta == 1.0:
                    sim_results['delta'] = '1'

                df = pd.concat([df, sim_results], ignore_index=True)
                del sim_results

            #Drop the rows of df with c values we don't want to include in the plot
            df = df[~df['c'].isin(drop_cs)]

            #Generate pointplot for visualization for this delta value
            sns.lineplot(x ='c', y = key, ax = ax, data = df, errorbar=error_type,
                         hue = "delta",  palette = colorlist,
                         style = "delta", markers = markerlist, dashes = False, markersize=10)

            title = r"$\gamma = $" + str(gamma)
            ax.set_title(title, fontsize = fontsizes['L'], pad = fontsizes['XS'])

            if row == 1 and col == 1:
                ax.set_xlabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['XXL'], labelpad = fontsizes["space"])
            else:
                ax.set(xlabel=None)
            ax.set(ylabel=None)

            ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
            if plot_max_c:
                ax.tick_params(axis = 'x', rotation = 45)
            ax.set_xlim(min_c, max_c)
            if custom_xticks:
                ax.set_xticks(xtick_list)
            if key in ['frac_edges', 'wt_avg_frac_edges_cluster', 'deleted_within_of_total', 'deleted_within_of_deleted']:
                ax.set_ylim(-0.05, 1.05)
                ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

            #Hide the legend
            ax.get_legend().remove()

            ax.text(-0.08, 1.2, idx_to_letter[label_idx], transform=ax.transAxes, fontsize=fontsizes['XL'], fontweight='bold', va='top', ha='right')

            #For the first gamma plot we generate, create a global legend in the last corner
            if row == 0 and col == 1:
                legend = fig.legend(title = r"$\delta$", loc='center left', bbox_to_anchor=bbox_to_anchor,
                          fontsize = fontsizes['M'], title_fontsize = fontsizes['L'], markerscale=2)
    if show_plot_title:
        if network_dataset:
            # suptitle = plot_title[key] + f" for {graph_name} Network"
            suptitle = f"{graph_name} Network"
        else:
            if graph_type == "complete":                
                suptitle = f'Complete({n})'
            elif graph_type == "erdos-renyi":
                suptitle = f'G({n}, {p})'
            elif graph_type == "SBM":
                suptitle = r"SBM($p_{aa}=p_{bb}=$" + str(p_aa) + r", $p_{ab}=$" + str(p_ab) + ")"
        fig.suptitle(suptitle, fontsize = fontsizes['XL'])

    # fig.supylabel(plot_ylabel[key], fontsize = fontsizes['XXL'], va='center')
    # fig.supxlabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['XXL'])

    #Save the plot
    savefile = directory + '/'
    if not network_dataset:
        savefile = savefile + f"{graph_type}{n}"
        if graph_type == "erdos-renyi":
            savefile = savefile + f"--p{p}"
        elif graph_type == "SBM":
            savefile = savefile + f"--p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}"
        if error_type == "sd":
            savefile = savefile + f"--{plot_name[key]}.png"
        else:
            savefile = savefile + f"--{plot_name[key]}--{error_type}.png"

    else:
        if error_type == "sd":
            savefile = savefile + f"{graph_name}--{plot_name[key]}.png"
        else:
            savefile = savefile + f"{graph_name}--{plot_name[key]}--{error_type}.png"

    # fig.subplots_adjust(left=0.05, right = 0.95, bottom=0.05, top = 0.95, wspace = 0.2, hspace = 0.2)
    fig.tight_layout(w_pad=4, h_pad=3)
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    # plt.show()
    plt.close()