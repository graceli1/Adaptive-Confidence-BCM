'''
Author: Grace Li
Date: 5/10/2022

This script runs simulations of our adaptive-confidence HK model on synthetic graphs. The options are
complete graphs, Erdos-Renyi G(n,p) random graphs, and stochastic block model (SBM) random graphs. 

To run this script, at the bottom, modifiy the variables that give the desired graph, number of nodes, random graph 
parameters (if applicable), and model parameters (confidence-increase parameter gamma, confidence-decrease parameter delta, 
and initial confidence bound c - this is called c0 in the paper).

For repeatability of the simulations, random seeds for generating random graphs are stored in graph_seeds.csv and 
random seeds for generating sets of initial opinions are stored in opinion_seeds.csv in the folder for each graph type.

For each parameter combination of:
    - random-graph parameters (if applicable)
    - graph number (for random graphs)
    - confidence-increase parameter gamma
    - confidence-decrease parameter delta
    - initial confidence bound c (called c0 in the paper)
    - opinion set (abbreviated to op in the code), 
the simulation results are stored in a matfile. The script MatfileConsolidator.py is used to calculate the quantities 
we examine and store them in a csv format. After running MatfileConsolidator.py, the various linePlots scripts are used 
to generate plots of our results.

IMPORTANT: This script was run with igraph version 0.9.7. For repeatability of the random graphs generated, it is important
to use the same version of igraph. The adjacency matrices for the random graphs generated are stored in the adjacency_matrix
folder for the Erdos-Renyi and SBM folders. One could also directly use those files to reproduce the random graphs we 
generated for our model simulations.

'''


# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority
import psutil
psutil.Process().nice(1)# if on *ux

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

# Class for running sets of HK experiments            
class HK_experiment:
    
    #Set class parameters
    tol = 1e-10 #1e-6 #Diameter required for convergence critera of opinion clusters
    Tmax = 10**6 #Bailout time for ending the simulation
    
    #Folder names by graph type for saving outputs
    folder_names = {'complete': 'Complete', 'erdos-renyi': 'Erdos-Renyi', "SBM":"SBM"}
    
    random_graph_types = ["erdos-renyi", "SBM"]
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, graph_type, n, p=False, pref_matrix=False, block_sizes=False):
        '''
        Initializes class to run Deffaunt-Weisbuch simulations for a particular graph type
        
        Parameters
        ----------
        graph_type : string
            String specifying the graph type. Currently the options are "complete" and "erdos-renyi"
        n : int
            Number of nodes in the graph(s) considered
        p : float, required only if graph_type == "erdos-renyi"
            If the graph_type is "erdos-renyi", then p is a required parameter. p is the edge probability
            in the G(n,p) Erdos-Renyi model.
        block_sizes : list 
            If the graph_type is an SBM, then block_sizes is a required parameter. 
            It is a list of the number of vertices in each block of the SBM adjacency matrix.
            See the igraph.Graph.SBM function for more details.
        pref_matrix : list of lists
            If the graph_type is an SBM, then pref_matrix is a required parameter. 
            It a matrix (in the form of a list of lists) of edge probability between/within each block in the SBM.
            See the igraph.Graph.SBM function for more details.
        '''
        
        self.graph_type = graph_type
        self.n = n
        
        #savefolder name for experiment
        self.foldername = self.folder_names[graph_type] + "/" + graph_type + str(n)
        
        #Check that a directory for this experiment exists, and if not, create it
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            os.makedirs(self.foldername + '/matfiles')
            os.makedirs(self.foldername + '/txtfiles')
        
        if self.graph_type == "erdos-renyi":
            self.p = p
            
        if self.graph_type == "SBM":
            self.block_sizes = block_sizes
            self.pref_matrix = pref_matrix
            #TODO If block_sizes == False or pref_matrix == False raise error

        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for random graphs (if not complete), and initial opinions if they don't exist yet
        '''
        
        self.graph_seed_file = self.foldername + "/graph_seeds.csv"
        self.opinion_seed_file = self.foldername + "/opinion_seeds.csv"

        if self.graph_type == "complete":
                
            #There is only one opinion seed for a complete graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['opinion_seed'])
                random.seed(a=None) #reset random by seeding it with the current time
                weight_seed = str(random.randrange(sys.maxsize))
                df.loc[0] = [weight_seed]
                df.to_csv(self.opinion_seed_file, index=False, header=True)
         
        elif self.graph_type == "erdos-renyi":
            
            #There is only graph seed per p value for an erdos-renyi graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.graph_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph_seed'])
                df.to_csv(self.graph_seed_file, index=False, header=True)
            df = pd.read_csv(self.graph_seed_file)
            row = df[df['p'] == self.p]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                graph_seed = str(random.randrange(sys.maxsize))
                row = pd.DataFrame(columns = ['p', 'graph_seed'])
                row.loc[0] = [self.p, graph_seed]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.graph_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
                
        elif self.graph_type == "SBM":
            
            #There is only one graph seed per type of SBM, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.graph_seed_file):
                df = pd.DataFrame(columns = ['p_aa', 'p_ab', 'p_bb', 'graph_seed'])
                df.to_csv(self.graph_seed_file, index=False, header=True)
            df = pd.read_csv(self.graph_seed_file)
            row = df[df['p_aa'] == self.pref_matrix[0][0]]
            row = row[row['p_ab'] == self.pref_matrix[0][1]]
            row = row[row['p_bb'] == self.pref_matrix[1][1]] 
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                graph_seed = str(random.randrange(sys.maxsize))
                row = pd.DataFrame(columns = ['p_aa', 'p_ab', 'p_bb', 'graph_seed'])
                row.loc[0] = [self.pref_matrix[0][0], self.pref_matrix[0][1], self.pref_matrix[1][1], graph_seed]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.graph_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['p_aa', 'p_ab', 'p_bb', 'graph', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_HK(self, params):
    
        '''
        Runs HK experiment and saves appropriate output files
        Takes in a dictionary params, containing
        "c" - the initial confidence bound,
        "delta" - the confidence-decrease parameter, "gamma" - the confidence-increase parameter,
        and "opinion_set" - an integer representing which opinion set to generate from 
        the random opinion seed to run the DW model on. 
        For a complete graph, we only need these parameters.
        For Erdos-Renyi and SBM graphs, the 5th parameter, "graph_number" needs to be specified,
        and it represents which randomly generated graph to consider.
        '''
        
        print('Process Number ', getpid())
        print('Params', params)

        ## Initial set up
        #Unpack parameters
        c = params["c"]
        delta, gamma = params["delta"], params["gamma"]
        opinion_set = params["opinion_set"]
        if self.graph_type in self.random_graph_types:
            graph_number = params["graph_number"]

        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #Make sure the appropriate save folders for this delta-gamma combo exist, and if not, create them
        subfolder = 'delta' + str(delta) + '-gamma' + str(gamma)
        if self.graph_type == 'erdos-renyi':
            subfolder = 'p' + str(self.p) + '/graph' + str(graph_number) + '/' + subfolder
        elif self.graph_type == "SBM":
            subfolder = f"p_aa{self.pref_matrix[0][0]}-p_bb{self.pref_matrix[1][1]}-p_ab{self.pref_matrix[0][1]}/graph{graph_number}/" + subfolder
        if not os.path.exists(self.foldername + "/matfiles/" + subfolder):
            os.makedirs(self.foldername + '/matfiles/' + subfolder)
        if not os.path.exists(self.foldername + "/txtfiles/" + subfolder):
            os.makedirs(self.foldername + '/txtfiles/' + subfolder)
        
        # Get the random graph seed if its a random graph
        if self.graph_type in self.random_graph_types:
            df = pd.read_csv(self.graph_seed_file)
            if self.graph_type == 'erdos-renyi':
                df = df[df['p'] == self.p]
            elif self.graph_type == "SBM":
                df = df[df['p_aa'] == self.pref_matrix[0][0]]
                df = df[df['p_ab'] == self.pref_matrix[0][1]]
                df = df[df['p_bb'] == self.pref_matrix[1][1]]  
            graph_seed = df['graph_seed'].values[0]
            graph_seed = int(graph_seed)
            
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        if self.graph_type == 'complete':
            opinion_seed = df['opinion_seed'].values[0]
            opinion_seed = int(opinion_seed)
        elif self.graph_type == 'erdos-renyi':
            row = df[df['p'] == self.p]
            row = row[row['graph'] == graph_number]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                opinion_seed = random.randrange(sys.maxsize)
                row = pd.DataFrame(columns = ['p', 'graph', 'opinion_seed'])
                row.loc[0] = [self.p, graph_number, str(opinion_seed)]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            else:
                opinion_seed = row['opinion_seed'].values[0]
                opinion_seed = int(opinion_seed)
        elif self.graph_type == "SBM":
            row = df[df['p_aa'] == self.pref_matrix[0][0]]
            row = row[row['p_ab'] == self.pref_matrix[0][1]]
            row = row[row['p_bb'] == self.pref_matrix[1][1]]  
            row = row[row['graph'] == graph_number]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                opinion_seed = random.randrange(sys.maxsize)
                row = pd.DataFrame(columns = ['p_aa', 'p_ab', 'p_bb', 'graph', 'opinion_seed'])
                row.loc[0] = [self.pref_matrix[0][0], self.pref_matrix[0][1], self.pref_matrix[1][1], graph_number, str(opinion_seed)]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            else:
                opinion_seed = row['opinion_seed'].values[0]
                opinion_seed = int(opinion_seed)
        lock.release()
        
        ## Specify the save file names for matfiles and txtfiles
        savename = ""
        if self.graph_type == 'erdos-renyi':            
            savename = savename + 'p' + str(self.p) + '-graph' + str(graph_number) + "--"
        elif self.graph_type == "SBM":
            savename = savename + 'graph' + str(graph_number) + "--"
        savename = savename + 'delta' + str(delta) + '-gamma' + str(gamma) + '--c' + str(c)
        txtfile = self.foldername + '/txtfiles/' + subfolder + '/' + savename + '.txt'
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_type, ", n =", self.n, file=f, flush=True)
                if self.graph_type == 'erdos-renyi':
                    print("p =", self.p, ", graph_number = ", graph_number, file=f, flush=True)
                    print('graph_seed = ', graph_seed, file=f, flush=True)
                elif self.graph_type == "SBM":
                    print("pref_matrix =", self.pref_matrix, ", graph_number = ", graph_number, file=f, flush=True)
                    print('graph_seed = ', graph_seed, file=f, flush=True)
                print('delta = ', delta, ' and gamma = ', gamma, file=f, flush = True)
                print('c = ', c, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
        
        # Generate graph
        if self.graph_type == "complete":
            G = igraph.Graph.Full(self.n)
        elif self.graph_type == "erdos-renyi":
            #Reinitialize the random seed and generate the corresponding graph number from that seed
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G = igraph.Graph.Erdos_Renyi(self.n, self.p)
        elif self.graph_type == "SBM":
            #Reinitialize the random seed and generate the corresponding graph number from that seed
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G = igraph.Graph.SBM(self.n, self.pref_matrix, self.block_sizes, directed=False, loops=False)
                
        #Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        #Time the HK simulation for this weight + opinion set combo
        start_time = time.time()

        ## Run the DW model using the simulation seed
        # print('Process Number ', getpid(), 'starting HK') #deleteline
        outputs = AdaptiveConfidenceBCM.HK(G, c, delta, gamma,
                         tol = self.tol, Tmax = self.Tmax)
        # print('Process Number ', getpid(), 'finished HK') #deleteline

        # Dump model outputs into file
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- Opinion_set = %s -----" % opinion_set, file=f, flush=True)

            print("T = %s" % outputs['T'], file=f, flush=True)
            print("Min confidence bound = %.3f, and Max confidence bound = %.3f" % (min(outputs['confidence']), max(outputs['confidence'])), file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'c': c, 'delta': delta, 'gamma':gamma, 'opinion_set': opinion_set}
        
        #Include the graph-level results
        save_sim['T'] = outputs['T']       
        save_sim['T_acc'] = outputs['T_acc']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']
        
        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()
            
        #Include the node-level results as size n arrays
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change']
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        #Edge level information
        save_sim['confidence'] = outputs['confidence']
        
        ## Save the simulation results to a matfile
        matfile = self.foldername + '/matfiles/' + subfolder + '/' + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
        
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    graph_type = "SBM"
    
    ns = [1000] #[1000, 2000] #graph size(s)

    # Edge probability p for the Erdos-Renyi  G(n,p) model. Change here:
    p = False
    if graph_type == "erdos-renyi":
        p = 0.5
    
    # Change SBM parameters here:
    #Edge probabilities
    pref_matrix = False
    block_sizes = False
    if graph_type == "SBM":
        p_aa, p_bb = 1, 1
        p_ab = 0.01 #0.1
        pref_matrix = [[p_aa, p_ab], [p_ab, p_bb]]
        
    #Confidence-increase parameter
    # gammas = [0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]
    gammas = [0.01, 0.005, 0.001, 0.05]
    
    #Confidence-decrease parameter
    deltas = [0.5, 0.9, 0.95, 0.99, 1.0]

    delta_gammas = [(1.0, 0.0)]
    
    for delta in deltas:
        for gamma in gammas:
            delta_gammas.append((delta, gamma))
    
    #Initial confidence bound
    cs = [0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.3, 0.4, 0.5]
    
    #Specify which graphs to look at if using a random-graph model
    graph_numbers = list(range(0,5))
    if graph_type == "complete":
        graph_numbers = [0] #if we have complete graphs, there is only one graph to look at
    
    opinion_sets = list(range(0,10)) #Which opinion sets to run
    
    # folder_names = {'complete': 'Complete', 'erdos-renyi': 'Erdos-Renyi', 'SBM-2-community': 'SBM', 'SBM-core-periphery': 'SBM'}
    
    for n in ns:
        
        if graph_type == "SBM":
            #Block sizes
            A = int(n * 0.75)
            B = int(n * 0.25)
            block_sizes = [A, B]

        ## Generate list of tuples to feed into DW_experiments as parameters
        params_list = []
        for graph_number in graph_numbers:
            for pair in delta_gammas:
                delta = pair[0]
                gamma = pair[1]
                for c in cs:
                    for opinion_set in opinion_sets:

                        #Only run the simulation with these parameters if we have not already done so
                        #Get the matfile name for these parameters and check it
                        if graph_type == "complete":
                            matfile = (
                                        f"Complete/complete{n}/matfiles/delta{delta}-gamma{gamma}"
                                        f"/delta{delta}-gamma{gamma}--c{c}-op{opinion_set}.mat"
                                      )
                        elif graph_type == "erdos-renyi":
                            matfile = (
                                        f"Erdos-Renyi/erdos-renyi{n}/matfiles/p{p}"
                                        f"/graph{graph_number}/delta{delta}-gamma{gamma}"
                                        f"/p{p}-graph{graph_number}--delta{delta}-gamma{gamma}"
                                        f"--c{c}-op{opinion_set}.mat"
                                      )
                        elif graph_type == "SBM":
                            matfile = (
                                        f"SBM/SBM{n}/matfiles/p_aa{p_aa}-p_bb{p_bb}-p_ab{p_ab}"
                                        f"/graph{graph_number}/delta{delta}-gamma{gamma}"
                                        f"/graph{graph_number}--delta{delta}-gamma{gamma}"
                                        f"--c{c}-op{opinion_set}.mat"
                                      )
                        try:
                            results = io.loadmat(matfile)

                        except:
                            # print(matfile)
                            param_dict = {"delta": delta, "gamma": gamma,
                                          "c": c, "opinion_set": opinion_set}
                            if graph_type != "complete":
                                param_dict["graph_number"] = graph_number
                            params_list.append(param_dict) 

        #Initialize experiment class
        experiment = HK_experiment(graph_type, n = n, p = p, block_sizes=block_sizes, pref_matrix=pref_matrix)
        experiment.generate_seed_files()

        l = multiprocessing.Lock()

        with multiprocessing.Pool(processes=35, initializer=init, initargs=(l,)) as pool:
            pool.map(experiment.run_HK, params_list)