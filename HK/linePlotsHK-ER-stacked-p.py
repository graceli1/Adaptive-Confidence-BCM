'''
Author: Grace Li
Date: 11/1/2022

This script generates plots of the results of the simulations for our adaptive-confidence HK BCM.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, W(T), and convergence time. 
Here, we also plot the variance of final opinions, mean local receptiveness, fraction of edges in the effective graph. 
and the fraction of deleted edges overall in the effective graph and within clusters of the effective graph.
For each quantity of interest, this script generates a single figure where each plot shows the quantity of interest
versus the initial confidence bound c0 (called c in the code). Each plot represents a single confidence-increase parameter 
gamma value. Each plot has different colored curves corresponding to different confidence-decrease parameter delta values. 
We plot the mean with error range representing one standard deviation. We generate each point on the plot from 50 numerical 
simulations from 5 randomly drawn graphs each with 10 sets of initial opinions drawn uniformly at random.


We used this script to generate our figures for our simulations of our adaptive-confidence HK model on Erdos-Renyi graphs.
It generates figures where the left half of the figure contains all plots for ER graphs with p = 0.1 and the right half
of the figure contains all plots for ER graphs with p = 0.5. 

'''

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# # Import our own adaptive-confidence bounded-confidence module
# import sys
# sys.path.append('..') #look one directory above
# import AdaptiveConfidenceBCM

sns.set_style("ticks",{'axes.grid' : True})

#Change experiment parameters here -----------------------------------------------
#Graph type

#Options are: “se” for standard error or "sd" for standard deviation
error_type = "sd"

#Whether to look at the smaller/more interesting plot, or plot to the maximum c value for the x-axis
plot_max_c = False

graph_type = "erdos-renyi"
n = 1000
ps = [0.1, 0.5]

#Whether or not to also plot the baseline HK model
no_baseline = False

runtoTmax = False
toltest = False
tol = 0.01 #specify the tolerance value if its not the standard

#Specify which quantities to plot
keys = [
        'log(T)', 'entropy',
        'n_major', 'n_minor',
        'wt_avg_frac_edges_cluster'
       ]
if no_baseline:
    keys = ['wt_avg_frac_edges_cluster']

#Confidence-increase parameter
gammas = [0.001, 0.005, 0.01, 0.05]

#Confidence-decrease parameter
deltas = [0.5, 0.9, 0.95, 0.99, 1.0]
legend_deltas = [0.5, 0.9, 0.95, 0.99, 1]
#Colors and markers that correspond to each delta value
colorlist = ['#0173b2', '#de8f05', '#029e73', '#d55e00', '#56b4e9']
markerlist = ["o", "X", "s", "^", "*"]

#Initial confidence bound
cs = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.3, 0.4, 0.5]

min_c = 0.018
max_c = 0.202
custom_xticks = True
xtick_list = [0.02, 0.05, 0.10, 0.15, 0.20]
drop_cs = [0.01]
if plot_max_c:
    max_c = max(cs) + 0.002
    drop_cs = [0.01]
    xtick_list = [0.05, 0.10, 0.15, 0.20, 0.3, 0.4, 0.5]

#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi", "SBM":"SBM"}
folder_name = folder_name_dict[graph_type]
random_graph_folders = ["Erdos-Renyi", "SBM"] #The names of the folders with random-graph models



#Plot parameters  ---------------------------------------------------------------------------
#Plot font size parameters
fontsizes = {'XS': 13, 'S': 15, 'M': 24, 'L':25, 'XL': 35, 'XXL': 45, 'p_label': 50, 'space':18, 'left_space': 50}
# if no_baseline:
#     fontsizes['left_space'] = 120

plot_height = 5
if no_baseline:
    plot_height = 5.7
plot_width = 5.2
wspace = 0.1

rows, cols = 3, 2
bbox_to_anchor=(1.01, 0.53)

if no_baseline:
    rows = rows - 1

plot_name = {
                'log(T)': 'T',
                'n_clusters': 'clusters', 
                'n_minor': 'n_minor',
                'n_major': 'n_major',
                'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis',
                'frac_edges': 'frac_edges', 
                'wt_avg_frac_edges_cluster': 'wt_avg_frac_edges',
                'deleted_within_of_total': 'deleted_within_of_total',
                'deleted_within_of_deleted': 'deleted_within_of_deleted'
            }
plot_ylabel = {
                'log(T)': r"$\log_{10}(T_f)$", 
                'n_clusters': 'Number of clusters',
                'n_minor': 'Number of minor clusters',
                'n_major': 'Number of major clusters',
                'entropy': 'Shannon entropy ' + r"$H(T_f)$",
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis',
                'frac_edges': 'Fraction of edges', 
                'wt_avg_frac_edges_cluster': r"$W(T_f)$",
                'deleted_within_of_total': 'Fraction of\ndisconnected edges',
                'deleted_within_of_deleted': 'Fraction of within-cluster\ndisconnected edges'
             }

plot_title = plot_ylabel.copy()
plot_title['deleted_within_of_total'] = 'Fraction of disconnected edges'
plot_title['deleted_within_of_deleted'] = 'Fraction of within-cluster disconnected edges'

#Names for the columns of the consolidated matfiles
columns = ['delta', 'gamma', 'c', 'opinion_set',
           'n_clusters', 'n_major', 'n_minor',
           'max_diameter', 'T', 'bailout', 
           'avg_opinion_diff', 'avg_local_receptiveness', 'entropy',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis',
           'frac_edges', 'wt_avg_frac_edges_cluster',
           'deleted_within_of_total', 'deleted_within_of_deleted'
          ]

idx_to_letter = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 8:'I', 9:'J', 10:'K'}

# Plot results for each gamma/delta pair ---------------------------------------------------

    
experiment = f"Erdos-Renyi/erdos-renyi{n}" + ("/runtoTmax" * runtoTmax) #savefolder name for experiment

#Check that a directory exists, and if not, create it
directory = experiment + f"/combined_plots/stacked_p_{error_type}"
if plot_max_c:
    directory = directory + "/plot_max_c"
if not os.path.exists(directory):
    os.makedirs(directory)

#Loop through the quantities of interest
for key in keys:
    print(key)
    
    if no_baseline:
        fig = plt.figure(constrained_layout=True, figsize=(plot_width*(cols*len(ps) + wspace), plot_height*rows) )
        subfigs = fig.subfigures(1, len(ps), wspace = wspace)
    else:
        title_width_adjust = 0.11
        fig = plt.figure(constrained_layout=True, figsize=(plot_width*(cols*len(ps) + wspace + title_width_adjust), plot_height*rows) )
        subfigs = fig.subfigures(1, len(ps), width_ratios = [1+title_width_adjust, 1], wspace = wspace)
    
    for subfig_idx in range(len(ps)):
    
        p = ps[subfig_idx]

        axs = subfigs[subfig_idx].subplots(rows, cols)
        
        #Generate each subplot
        for row in range(rows):
            for col in range(cols):
                
                ax_idx = row * cols + col
                ax = axs[row, col]
                
                if no_baseline:
                    #Label this subfigure
                    label = " "*3 +  r"$p$ = " + str(p) + "\n"
                    subfigs[subfig_idx].suptitle(label, fontsize=fontsizes['p_label'])
                
                if not no_baseline:
                    
                    #Label this subfigure
                    if row == 0 and col == 0:
                        ax.set_axis_off()
                        label = r"$p$ = " + str(p) + " "
                        ax.text(0.5, 0.5, label, ha="center", va="center", fontsize=fontsizes['p_label'])

                    #Plot the standard HK model (gamma = 0, delta = 1) in the first plot
                    elif row == 0 and col == 1:

                        delta, gamma = 1.0, 0.0

                        #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
                        filename = experiment + f'/combined_results/p{p}'
                        filename = filename + '/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
                        sim_results = pd.read_csv(filename)

                        #Make sure the opinion_set, and time steps are integers instead of floats
                        sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'n_clusters': float})

                        #Calculate the log of time steps to make it easier to visualize
                        sim_results['log(T)'] = np.log10(sim_results['T'])

                        #Drop the rows with c values we don't want to plot
                        sim_results = sim_results[~sim_results['c'].isin(drop_cs)]

                        #Generate pointplot for visualization for this delta value
                        # sns.pointplot(x='c', y=key, ax = ax, data=sim_results)
                        sns.lineplot(x='c', y=key, ax = ax, data=sim_results, errorbar=error_type,
                                     hue = "delta", palette = ["black"], style = "delta", 
                                     markers = ["."], markersize=10, legend=False)

                        title = "Baseline HK model\n" + r"($\gamma = 0$, $\delta = 1$)"
                        ax.set_title(title, fontsize = fontsizes['L'], pad=fontsizes['XS'])

                        ax.set_xlabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['space'])
                        if col == 0:
                            ax.set_ylabel(plot_ylabel[key], fontsize = fontsizes['left_space'])
                        else:
                            ax.set_ylabel(plot_ylabel[key], fontsize = fontsizes['space'])
                        ax.xaxis.label.set_color('white')
                        ax.yaxis.label.set_color('white')
                        # ax.set(xlabel=None)
                        # ax.set(ylabel=None)
                        ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
                        ax.set_xlim(min_c, max_c)
                        if plot_max_c:
                            ax.tick_params(axis = 'x', rotation = 45)
                        if custom_xticks:
                            ax.set_xticks(xtick_list)
                        if key in ['frac_edges', 'wt_avg_frac_edges_cluster', 'deleted_within_of_total', 'deleted_within_of_deleted']:
                            ax.set_ylim(-0.05, 1.05)
                            ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

                #Otherwise plot a subplot of all our simulations for one gamma value
                if no_baseline or (not no_baseline and row != 0):

                    #Get the gamma value for this plot
                    if no_baseline:
                        gamma_idx = ax_idx
                    else:
                        gamma_idx = ax_idx - 2
                    gamma = gammas[gamma_idx]

                    #Initialize the dataframe to concatenate results for all the delta values for this particular gamma
                    df = pd.DataFrame(columns = columns)

                    for delta in deltas:

                        #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
                        filename = experiment + f'/combined_results/p{p}'
                        filename = filename + '/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
                        sim_results = pd.read_csv(filename)
                        
                        #Make sure the opinion_set, and time steps are integers instead of floats
                        sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'n_clusters': float})

                        #Calculate the log of time steps to make it easier to visualize
                        sim_results['log(T)'] = np.log10(sim_results['T'])
                        
                        #Convert delta to a string so the legend displays properly
                        sim_results['delta'] = str(delta)
                        if delta == 1.0:
                            sim_results['delta'] = '1'
                        
                        df = pd.concat([df, sim_results], ignore_index=True)
                        del sim_results
                        
                    #Drop the rows of df with c values we don't want to include in the plot
                    df = df[~df['c'].isin(drop_cs)]

                    #Generate pointplot for visualization for this delta value
                    sns.lineplot(x ='c', y = key, ax = ax, data = df, errorbar="sd",
                                 hue = "delta",  palette = colorlist,
                                 style = "delta", markers = markerlist, dashes = False, markersize=10)

                    title = r"$\gamma = $" + str(gamma)
                    ax.set_title(title, fontsize = fontsizes['L'], pad=fontsizes['XS'])

                    ax.set_xlabel(" ", fontsize = fontsizes['space'])
                    ax.xaxis.label.set_color('white')
                    if subfig_idx == 0 and col == 0 and not no_baseline:
                        if row == 1:
                            ax.set_ylabel(plot_ylabel[key], fontsize = fontsizes['XXL'], va='center', labelpad = 100)
                        else:
                            ax.set(ylabel=None)
                    elif col == 0:
                        ax.set_ylabel(" ", fontsize = fontsizes['left_space'])
                        ax.yaxis.label.set_color('white')
                    else:
                        ax.set_ylabel(" ", fontsize = fontsizes['space'])
                        ax.yaxis.label.set_color('white')
                    # ax.set(xlabel=None)
                    # ax.set(ylabel=None)
                    ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
                    # ax.tick_params(axis = 'x', rotation = 45)
                    ax.set_xlim(min_c, max_c)
                    if plot_max_c:
                        ax.tick_params(axis = 'x', rotation = 45)
                    if custom_xticks:
                        ax.set_xticks(xtick_list)
                    if key in ['frac_edges', 'wt_avg_frac_edges_cluster', 'deleted_within_of_total', 'deleted_within_of_deleted']:
                        ax.set_ylim(-0.05, 1.05)
                        ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

                    #Hide the legend
                    ax.get_legend().remove()

                    #For the first gamma plot we generate, create a global legend in the last corner
                    if subfig_idx == 0 and gamma_idx == 0:
                        legend = fig.legend(title = r"$\delta$", loc='center left', bbox_to_anchor=bbox_to_anchor,
                                            fontsize = fontsizes['L'], title_fontsize = fontsizes['XL'], markerscale=2)
                        
                #Label each figure panel
                if no_baseline:
                    # panel_label = str(subfig_idx+1) + idx_to_letter[ax_idx]
                    panel_label = idx_to_letter[ax_idx + subfig_idx * len(gammas)]
                    ax.text(-0.08, 1.2, panel_label, transform=ax.transAxes, fontsize=fontsizes['XL'], fontweight='bold', va='top', ha='right')
                elif not (row == 0 and col == 0):
                    # panel_label = str(subfig_idx+1) + idx_to_letter[ax_idx - 1]
                    panel_label = idx_to_letter[ax_idx - 1 + subfig_idx * (len(gammas) + 1)]
                    ax.text(-0.08, 1.2, panel_label, transform=ax.transAxes, fontsize=fontsizes['XL'], fontweight='bold', va='top', ha='right')

        subfigs[subfig_idx].supxlabel(r"    Initial confidence bound ($c_0$)", fontsize = fontsizes['XXL'])
        
    suptitle = plot_title[key] + f' for G({n}, p)'
    # fig.suptitle(suptitle, fontsize = fontsizes['XL'])
    
    if no_baseline:
        fig.supylabel(plot_ylabel[key], fontsize = fontsizes['XXL'], va='center')
        fig.suptitle(" ", fontsize = fontsizes['XS'])
        
        
    #Add vertical line between subfigures for clarity
    line = plt.Line2D([0.535, 0.535],[0, 1], color="black", linewidth=2)
    fig.add_artist(line)
        
    savefile = directory + f"/erdos_renyi{n}--{plot_name[key]}"
    if error_type != "sd":
        savefile = savefile + "--{error_type}"
    if no_baseline:
        savefile = savefile + "--no_baseline"
    savefile = savefile + ".png"
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    # plt.show()
    plt.close()