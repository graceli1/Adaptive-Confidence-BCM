'''
Author: Grace Li
Date: 6/26/2022

This script runs simulations of our adaptive-confidence HK model on the Facebook100 graphs. The graphs we 
examine are Reed, Swarthmore, Oberlin, Pepperdine, Rice, and UCSB. 

To run this script, at the bottom, modifiy the variables that give the desired graph name and model parameters 
(confidence-increase parameter gamma, confidence-decrease parameter delta, and initial confidence bound c - this is called c0 in the paper).

For repeatability of the simulations, random seeds for generating sets of initial opinions are stored in opinion_seeds.csv 
in the folder for each graph type.

For each parameter combination of:
    - confidence-increase parameter gamma
    - confidence-decrease parameter delta
    - initial confidence bound c (called c0 in the paper)
    - opinion set (abbreviated to op in the code), 
the simulation results are stored in a matfile. The script MatfileConsolidator.py is used to calculate the quantities we
examine and store them in a csv format. After running MatfileConsolidator.py, the various linePlots scripts are used to
generate plots of our results.

IMPORTANT: This script uses the syntax for igraph version 0.9.7 to get the largest connected component of each of the
Facebook100 network data sets to run our simulations on. If using a different igraph version, that part will need to be
changed.

'''

# Import required packages
import numpy as np
import pandas as pd
import scipy
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority if on *ux
import psutil
psutil.Process().nice(1)

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

# Class for running sets of HK experiments            
class HK_experiment:
    
    #Set class parameters
    tol = 1e-10 #1e-6 #Diameter required for convergence critera of opinion clusters
    Tmax = 10**6 #Bailout time for ending the simulation
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, G, graph_name):
        '''
        Initializes class to run Deffaunt-Weisbuch simulations for a particular graph type
        
        Parameters
        ----------
        G : igraph.graph
            igraph Graph of the graph structure to run our simulation on
        graph_name : string
            String specifying the graph name. This should be one of the Facebook100 university networks, or the "netscience" network
        '''
        
        self.G = G
        self.n = G.vcount() #get number of vertices
        
        self.graph_name = graph_name
        
        #savefolder name for experiment
        self.foldername = 'Facebook100/' + graph_name
        if graph_name == "netscience":
            self.foldername = "NetScience"
        
        #Check that a directory for this experiment exists, and if not, create it
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            os.makedirs(self.foldername + '/matfiles')
            os.makedirs(self.foldername + '/txtfiles')
            
        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for initial opinions if they don't exist yet
        '''
        
        self.opinion_seed_file = self.foldername + "/opinion_seeds.csv"
    
        #There is only one opinion seed for the fixed graph, so we generate and save it if it doesn't exist yet
        if not os.path.exists(self.opinion_seed_file):
            df = pd.DataFrame(columns = ['opinion_seed'])
            random.seed(a=None) #reset random by seeding it with the current time
            weight_seed = str(random.randrange(sys.maxsize))
            df.loc[0] = [weight_seed]
            df.to_csv(self.opinion_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_HK(self, params):
    
        '''
        Runs DW experiment and saves appropriate output files
        Takes in a dictionary params, containing
        "c" - the initial confidence bound,
        "delta" - the confidence-decrease parameter, "gamma" - the confidence-increase parameter,
        and "opinion_set" - an integer representing which opinion set to generate from 
        the random opinion seed to run the DW model on. 
        For a complete graph, we only need these parameters.
        For Erdos-Renyi graphs, the 5th parameter, "graph_number" needs to be specified,
        and it represents which randomly generated graph to consider.
        '''
        
        print('Process Number ', getpid())
        print('Params', params)
        
        ## Initial set up
        #Unpack parameters
        c = params["c"]
        delta, gamma = params["delta"], params["gamma"]
        opinion_set = params["opinion_set"]

        G = self.G.copy() #Make a function copy of the graph
        
        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #Make sure the appropriate save folders for this delta-gamma combo exist, and if not, create them
        subfolder = 'delta' + str(delta) + '-gamma' + str(gamma)
        if not os.path.exists(self.foldername + "/matfiles/" + subfolder):
            os.makedirs(self.foldername + '/matfiles/' + subfolder)
        if not os.path.exists(self.foldername + "/txtfiles/" + subfolder):
            os.makedirs(self.foldername + '/txtfiles/' + subfolder)
                
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        opinion_seed = df['opinion_seed'].values[0]
        opinion_seed = int(opinion_seed)
        
        lock.release()
        
        ## Specify the save file names for matfiles and txtfiles
        savename = 'delta' + str(delta) + '-gamma' + str(gamma) + '--c' + str(c)
        txtfile = self.foldername + '/txtfiles/' + subfolder + '/' + savename + '.txt'
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_name, file=f, flush=True)
                print('delta = ', delta, ' and gamma = ', gamma, file=f, flush = True)
                print('c = ', c, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
            
        #Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        #Time the HK simulation for this weight + opinion set combo
        start_time = time.time()

        ## Run the DW model using the simulation seed
        # print('Process Number ', getpid(), 'starting HK') #deleteline
        outputs = AdaptiveConfidenceBCM.HK(G, c, delta, gamma,
                         tol = self.tol, Tmax = self.Tmax)
        # print('Process Number ', getpid(), 'finished HK') #deleteline

        # Dump model outputs into file
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- Opinion_set = %s -----" % opinion_set, file=f, flush=True)

            print("T = %s" % outputs['T'], file=f, flush=True)
            print("Min confidence bound = %.3f, and Max confidence bound = %.3f" % (min(outputs['confidence']), max(outputs['confidence'])), file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'c': c, 'delta': delta, 'gamma':gamma, 'opinion_set': opinion_set}
        
        #Include the graph-level results
        save_sim['T'] = outputs['T']       
        save_sim['T_acc'] = outputs['T_acc']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']
        
        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()
            
        #Include the node-level results as size n arrays
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change']
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        #Edge level information
        save_sim['confidence'] = outputs['confidence']
        
        ## Save the simulation results to a matfile
        matfile = self.foldername + '/matfiles/' + subfolder + '/' + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
    
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    graph_names = ['Reed', 'Swarthmore', 'Oberlin', 'Pepperdine', 'Rice', 'UCSB']
    
    #Confidence-increase parameter
    gammas = [0.01, 0.005, 0.001, 0.05]
    
    #Confidence-decrease parameter
    deltas = [0.5, 0.9, 0.95, 0.99, 1.0]

    delta_gammas = [(1.0, 0.0)]
    
    for delta in deltas:
        for gamma in gammas:
            delta_gammas.append((delta, gamma))
    
    #Initial confidence bound
    cs = [0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.3, 0.4, 0.5]
    
    opinion_sets = list(range(0,10)) #Which opinion sets to run
    
    for graph_name in graph_names:
    
        print(graph_name)
        
        ## Load the network dataset and get the giant connected component
        if graph_name == "netscience":
                G = igraph.Graph.Read_GML('NetScience/network_data/netscience.gml')
        else:
            matfile = f"Facebook100/network_data/{graph_name}.mat"
            data = io.loadmat(matfile)
            A = data['A'].toarray()
            A = A.astype(int)
        
            # G = igraph.Graph.Adjacency(data['A'].toarray(), mode = "undirected")
            G = igraph.Graph.Adjacency(A, mode = "undirected")
            print('Original number of vertices:', G.vcount())
            del data

        # component_membership = G.clusters(mode='strong').membership
        # component = np.nonzero(np.array(component_membership)==0)[0]
        component_membership = G.clusters(mode='strong').membership
        sizes = G.clusters(mode='strong').sizes()
        index_max = sizes.index(max(sizes))
        component = np.nonzero(np.array(component_membership)==index_max)[0]

        G = G.induced_subgraph(component)
        print('GCC number of vertices:', G.vcount())
    

        ## Generate list of tuples to feed into DW_experiments as parameters
        params_list = []
        for pair in delta_gammas:
            delta = pair[0]
            gamma = pair[1]
            for c in cs:
                for opinion_set in opinion_sets:

                    #Only run the simulation with these parameters if we have not already done so
                    #Get the matfile name for these parameters and check it
                    if graph_name == "netscience":
                         matfile = (
                                     f"NetScience/matfiles/delta{delta}-gamma{gamma}" 
                                     f"/delta{delta}-gamma{gamma}--c{c}-op{opinion_set}.mat"
                                   )
                    else:
                        matfile = (
                                    f"Facebook100/{graph_name}/matfiles/delta{delta}-gamma{gamma}" 
                                    f"/delta{delta}-gamma{gamma}--c{c}-op{opinion_set}.mat"
                                  )
                    try:
                        results = io.loadmat(matfile)
                        # print(matfile)
                    except:
                        param_dict = {"delta": delta, "gamma": gamma,
                                      "c": c, "opinion_set": opinion_set}
                        params_list.append(param_dict) 

        #Initialize experiment class
        experiment = HK_experiment(G, graph_name)
        experiment.generate_seed_files()

        l = multiprocessing.Lock()

        with multiprocessing.Pool(processes=40, initializer=init, initargs=(l,)) as pool:
            pool.map(experiment.run_HK, params_list)