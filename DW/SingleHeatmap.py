'''
Author: Grace Li
Date: 4/12/2022

Created on 4/12/2022
Created by Grace Li

This script generates heatmaps of the results of the simulations for our adaptive-confidence DW BCM.
We do not use these heatmaps in the paper and switch to line plots instead for clearer visualizations.

For each quantity of interest and model parameters confidence-increase parameter gamma and confidence-decrease 
parameterr delta, this script generates a single heatmap. The horizontal axis of each heatmap has the compromise 
parameter mu, and the vertical axis of each heatmap has the initial confidence bound c0 (called c in the code).
We generate each point square on the heatmap from 10 numerical simulations with different sets of initial 
opinions drawn uniformly at random.

'''

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

#Specify the experiment ---------------------------------------
graph_name = "netscience" #If we have a specific graph
network_dataset = False #Set equal to true if we want to use graph name

graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 200

runtoTmax = False
toltest = False
tol = 0.001 #specify the tolerance value if its not the standard 0.02
zoomin = False
    
#Specify parameters for plotting --------------------------------------

#Parameters for plotting
decimal_places = 3
show_labels = True
print_std = True
generate_min_max = True

#Confidence-increase parameter
gammas = [0.1]

#Confidence-decrease parameter
deltas = [0.5]

#Initial confidence bound
cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
cs = np.flip(cs) #flip cs so that heatmaps are ordered properly

#Compromise parameter
mus = [0.1, 0.3, 0.5]

#Initial confidence bound
if zoomin:
    #cs = [0.1, 0.15, 0.2]
    cs = [0.1, 0.125, 0.15, 0.175, 0.2]
    cs = np.flip(cs) #flip cs so that heatmaps are ordered properly
    mus = [0.1, 0.2, 0.3, 0.4, 0.5]

#Specify which quantities to plot
keys = [
        'log(T)', 'log(T_changed)', 
        'n_clusters', 'entropy',
        'avg_opinion_diff', 
        'avg_local_receptiveness',
        'op_var'
       ]

#Title for plots based on the network
graph_titles = {'netscience': 'Netscience', 'karate': 'Karate Club', 'complete': 'Complete'}

#Plot fontsizes
fontsizes = {'XS': 14, 'S': 18, 'M': 20, 'L':25}

#Figure size
figsize = (10,10)
if zoomin:
    figsize = (12, 8)

#Colormap
nodes = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 0.9, 1.0]
colors = ['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58', '#002652'] #Yellow green blue
# colors = ['#fff7f3','#fde0dd','#fcc5c0','#fa9fb5','#f768a1','#dd3497','#ae017e','#7a0177','#49006a', '#0e0014'] #Red purple
base_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip the colors
reverse_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip back to the original in case we need them again

#savefolder name for experiment
if network_dataset:
    experiment = 'NetworkDatasets/' + graph_name + '_results' + ("/runtoTmax" * runtoTmax)
else:
    experiment = graph_type.capitalize() + '/' + graph_type + str(n) + ("/runtoTmax" * runtoTmax)
    
    
# Plot heat maps of all simulation values

# Translate the quantity into text for the savefile and plot titles using these dictionaries
plot_name = {
                'log(T)': 'T', 'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                'log(T)': r"$\log_{10}$(T)", 
                'log(T_changed)': r"$\log_{10}$(T_changed)",
                'n_clusters': 'Number of Clusters', 
                'entropy': 'Shannon Entropy',
                'avg_opinion_diff': 'Opinion Differences',
                'avg_local_agreement': 'Local Agreement', 
                'avg_local_receptiveness': 'Local Receptiveness', 
                'op_mean': 'Opinion Mean', 'op_var': 'Opinion Variance', 
                'op_skew': 'Opinion Skew', 'op_kurtosis': 'Opinion Kurtosis'
             }

title_prefix = {'avg':'Mean of ', 'std':'STD of ', 
                'min':'Minimum of ', 'max':'Maximum of '}

for delta in deltas:
    for gamma in gammas:

        #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
        filename = experiment + '/combined_results/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
        sim_results = pd.read_csv(filename)

        #Calculate the natural log of time steps to make it easier to visualize
        sim_results['log(T)'] = np.log10(sim_results['T'])
        sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

        #Check that a directory exists for storing heatmaps, and if not, create it
        plot_directory = (experiment + '/heatmaps/delta' 
                          + str(delta) + '-gamma' + str(gamma) + '/'
                          + ('zoomin/' * zoomin)
                          + ('toltest/' * toltest))
        if not os.path.exists(plot_directory):
            os.makedirs(plot_directory)
            os.makedirs(plot_directory + 'otherStats/')

        #Loop through the quantities of interest
        for key in keys:
            
            #Pick the colormap based on if we want higher or lower numbers more important
            if key in ['avg_local_receptiveness']: #lower numbers are more important
                cmap = reverse_cmap
            else:
                cmap = base_cmap

            #Create zero matricies for each heatmap we want to generate
            average, std = np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus)))
            if generate_min_max:
                minimum, maximum = np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus)))

            #Fill in the heatmap values 
            # rows correspond to d
            for row in range(0,len(cs)):
                c = cs[row]

                # columns correspond to mu
                for col in range(0, len(mus)):
                    mu = mus[col]

                    #filter the data table by the d and mu values
                    df = sim_results[sim_results['c'] == c]
                    df = df[df['mu'] == mu]

                    #Calculate the descriptive stats
                    stats = df[key].describe()

                    #Add the stats values to the appropriate matrix (heatmap)
                    average[row][col] = stats['mean']
                    std[row][col] = stats['std']
                    if generate_min_max:
                        minimum[row][col] = stats['min']
                        maximum[row][col] = stats['max']

            #Plot the heatmaps
            heatmaps = {'avg':average, 'std':std}
            if generate_min_max:
                heatmaps.update({'min':minimum, 'max':maximum})

            for stat in heatmaps.keys():
                heatmap = heatmaps[stat]
                
                #Savefile name for the plot
                savefile = (plot_directory + ('otherStats/' * (stat != 'avg'))
                            + (('tol' + str(tol) + '--') * toltest) 
                            + plot_name[key] + '_' + stat 
                            + ('_with_std' * print_std * (stat=='avg'))
                            + ('--no_labels' * (not show_labels)) + '.png')
                
                #If we're plotting the average and want to include std, then generate labels
                labels = None
                if print_std and stat == 'avg':    
                    #Get properly formatted precision on labels of avg +/- std
                    avg = heatmaps['avg']
                    std = heatmaps['std']
                    labels = AdaptiveConfidenceBCM.print_avg_with_std(avg, std)
                
                #Plot title
                if network_dataset:
                    title = graph_titles[graph_name] + '\n'
                else:
                    title = graph_titles[graph_type] +'(' + str(n) + ')\n'
                title = title + title_prefix[stat] + plot_title[key]

                AdaptiveConfidenceBCM.plot_heatmap(heatmap, cs=cs, mus=mus, cmap=cmap,
                                labels = labels, show_values = True, 
                                decimal_places = decimal_places, 
                                figsize = figsize, fontsizes = fontsizes,
                                title = title, savefile = savefile)
                    
                plt.close()
