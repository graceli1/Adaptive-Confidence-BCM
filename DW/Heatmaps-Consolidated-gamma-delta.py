'''
Author: Grace Li
Date: 4/12/2022

This script generates heatmaps of the results of the simulations for our adaptive-confidence DW BCM.
We do not use these heatmaps in the paper and switch to line plots instead for clearer visualizations.

For each quantity of interest, this script generates a single figure. Each figure has a heatmap for each (gamma, delta) 
confidence-increase and confidence-decrease parameter pairs. The horizontal axis of each heatmap has the compromise 
parameter mu, and the vertical axis of each heatmap has the initial confidence bound c0 (called c in the code).
We generate each point square on the heatmap from 10 numerical simulations with different sets of initial opinions 
drawn uniformly at random.

'''

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

#name of experiment folder
graph_name = "netscience" #If we have a specific graph
network_dataset = True #Set equal to true if we want to use graph name

graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 100
full_grid = False

decimal_places = 2
show_labels = True
print_error = True
#se for standard error, sd for standard deviation
error_type = "sd" 

vertical_cbar = True

runtoTmax = False
toltest = False
tol = 0.01 #specify the tolerance value if its not the standard 0.02

#savefolder name for experiment
if network_dataset:
    experiment = 'NetworkDatasets/' + graph_name + '_results' + ("/runtoTmax" * runtoTmax)
else:
    experiment = graph_type.capitalize() + '/' + graph_type + str(n) + ("/runtoTmax" * runtoTmax)

#Specify which quantities to plot
keys = [
        'log(T)', 'entropy',
        'n_major', 'n_minor',
        'wt_avg_frac_edges_cluster'
       ]

#Confidence-increase parameter
gammas = [0.1]

#Confidence-decrease parameter
deltas = [0.5]

if (not network_dataset) and (n == 100) and (graph_type == "complete"):
    if full_grid == True:
        gammas = [0.1, 0.3, 0.5] #Confidence-increase parameter
        deltas = [0.3, 0.5, 0.7] #Confidence-decrease parameter

#Initial confidence bound
cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
cs = np.flip(cs) #flip cs so that heatmaps are ordered properly

#Compromise parameter
mus = [0.1, 0.3, 0.5]

#Generate delta-gamma pairs for later
delta_gamma = [(1.0, 0.0)]
for delta in deltas:
    for gamma in gammas:
        delta_gamma.append((delta, gamma))

#Plot font size parameters
fontsizes = {'XS': 20, 'S': 25, 'M': 25, 'L':40, 'XL': 70} #for whole grid of plots
if len(gammas) == 1:
    fontsizes = {'XS': 20, 'S': 25, 'M': 25, 'L': 40, 'XL': 35} #for pair of plots

# cmap = "rainbow"
nodes = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 0.9, 1.0]
colors = ['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58', '#002652'] #Yellow green blue
# colors = ['#fff7f3','#fde0dd','#fcc5c0','#fa9fb5','#f768a1','#dd3497','#ae017e','#7a0177','#49006a', '#0e0014'] #Red purple
base_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip the colors
reverse_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))

#These are the keys that require plots printed in exponential notation and a wider figure size
#figsize = (fig_width * len(distributions), sum(subfig_heights)) #width x height
base_fig_width = 6
if print_error and error_type == "se":
    base_fig_width = 7
base_cbar_leftpad = 0.05
wider_figure_keys = ['avg_local_receptiveness', 'avg_opinion_diff', 'op_var']
wider_fig_width = base_fig_width + 2
wider_cbar_leftpad = 0

# We want to consider the graph-level results of various quantities
# Translate the quantity into text for the savefile and plot titles using these dictionaries

plot_name = {
                'log(T)': 'T',
                'n_clusters': 'clusters', 
                'n_minor': 'n_minor',
                'n_major': 'n_major',
                'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis',
                'frac_edges': 'frac_edges', 
                'wt_avg_frac_edges_cluster': 'wt_avg_frac_edges',
                'deleted_within_of_total': 'deleted_within_of_total',
                'deleted_within_of_deleted': 'deleted_within_of_deleted'
            }
plot_title = {
                'log(T)': r"$\log_{10}(T_f)$", 
                'n_clusters': 'Number of clusters',
                'n_minor': 'Number of minor clusters',
                'n_major': 'Number of major clusters',
                'entropy': 'Shannon entropy ' + r"$H(T_f)$",
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis',
                'frac_edges': 'Fraction of edges', 
                'wt_avg_frac_edges_cluster': r"$W(T_f)$",
                'deleted_within_of_total': 'Fraction of\ndisconnected edges',
                'deleted_within_of_deleted': 'Fraction of within-cluster\ndisconnected edges'
             }
# plot_title = plot_ylabel.copy()

# Plot heat maps of all simulation values

#Check that a directory exists, and if not, create it
directory = experiment + '/combined_plots/heatmaps'
if not os.path.exists(directory):
    os.makedirs(directory)

stats = ['mean', 'sd', 'se']
title_prefix = {'mean':'', 'sd': 'SD of ', 'se': 'SE of '}
save_suffix = {'mean':'', 'sd': '_sd', 'se': '_se'}

# ----------------------------------------------------
#Loop through the quantities of interest
for key in keys:
    
    print('Generating plot of ' + key)
    
    #Pick the figure size parameters based on the key
    fig_width = base_fig_width
    cbar_leftpad = base_cbar_leftpad
    if key in wider_figure_keys:
        fig_width = wider_fig_width
        cbar_leftpad = wider_cbar_leftpad
    figsize = (fig_width * (len(gammas) + 1), 8 * len(deltas))
    
    #Pick the colormap based on if we want higher or lower numbers more important
    if key in ['avg_local_receptiveness']: #lower numbers are more important
        cmap = reverse_cmap
    else:
        cmap = base_cmap

    heatmaps = {} #dictionary to store the heatmaps with axis c and mu for each delta-gamma combo
    
    for pair in delta_gamma:
        delta = pair[0]
        gamma = pair[1]
            
        #We will store the heatmaps in a dictionary with this key
        heatmap_key = 'delta=' + str(delta) + ', gamma=' + str(gamma)

        #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
        filename = experiment + '/combined_results/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
        sim_results = pd.read_csv(filename)

        #Make sure the opinion_set, and time steps are integers instead of floats
        sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'T_changed': int})

        #Calculate the log of time steps to make it easier to visualize
        sim_results['log(T)'] = np.log10(sim_results['T'])
        sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

        #Create zero matricies for each heatmap we want to generate
        mean, sd, se = np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus)))

        #Fill in the heatmap values 
        # rows correspond to d
        for row in range(0,len(cs)):
            c = cs[row]

            # columns correspond to mu
            for col in range(0, len(mus)):
                mu = mus[col]

                #filter the data table by the d and mu values
                df = sim_results[sim_results['c'] == c]
                df = df[df['mu'] == mu]

                #Calculate the descriptive stats
                stats = df[key].describe()

                #Add the stats values to the appropriate matrix (heatmap)
                mean[row][col] = stats['mean']
                sd[row][col] = stats['std']
                # se[row][col] = stats.sem(array)
                se[row][col] = stats['std'] / np.sqrt(len(df[key]))
                

        #store the heatmaps
        heatmaps[heatmap_key] = {'mean': mean, 'sd':sd, 'se': se}
    
    # ----------------------------------------------------
    #Plot the heatmaps
    for stat in ['mean']: #in heatmaps[distribution].keys():
        
        #plot_maps = [heatmaps[heatmap_key][stat] for heatmap_key in heatmap_keys] #collect all the heatmaps of the same quantity - i.e. average, sd
        plot_maps = {heatmap_key: heatmaps[heatmap_key][stat] for heatmap_key in heatmaps.keys()}  #collect all the heatmaps of the same quantity - i.e. average, sd
        fig, axs = plt.subplots(
                                len(deltas), len(gammas) + 1, sharey = True, sharex = True,
                                constrained_layout = True, figsize = figsize
                                ) #create plot
        #cbar_ax = fig.add_axes([1.01, .1, .02, .8]) #add colorbar - left, bottom, width, height
        # cbar_ax = fig.add_axes([.14, -0.08, .8, .06]) 
        cbar_ax = fig.add_axes([.1+cbar_leftpad, -0.08, .8, .05]) 
    
        #Get the maximum value for the colorbar
        vmax = [np.nanmax(plot_maps[heatmap_key]) for heatmap_key in plot_maps.keys()]
        vmax = AdaptiveConfidenceBCM.round_decimals_up(max(vmax), 1)
    
        #Get the minimum value for the color bar
        vmins = []
        for heatmap_key in plot_maps.keys():
            matrix = plot_maps[heatmap_key]
            # If we have -inf values, i.e. if a value is ln(0), then set to 0 so it will plot
            if (np.amin(matrix) == - np.inf):
                matrix[matrix == - np.inf] = 0
                plot_maps[heatmap_key] = matrix
                vmins.append(0)
            else:
                vmins.append(AdaptiveConfidenceBCM.round_decimals_down(np.nanmin(matrix), 1))
        vmin = min(vmins)
    
        # ----------------------------------------------------
        #PLot the heatmap for each delta--gamma combination
        for i in range(len(deltas)):
            for j in range(len(gammas)):
            
                #Get the heatmap key
                delta, gamma = deltas[i], gammas[j]
                heatmap_key = 'delta=' + str(delta) + ', gamma=' + str(gamma)
                
                if len(deltas) == 1:
                    ax = axs[j+1]
                else:
                    ax = axs[i, j+1] #single out the right axis

                matrix = plot_maps[heatmap_key] #get the matrix for the heatmap

                #Convert matrix to dataframe for axis labels
                df = pd.DataFrame(matrix, index = cs, columns = mus)

                #Plot heat map with or without entry values depending on show_values
                if show_labels:
                    #Get the annotated values of average +/- sd
                    if print_error and stat == 'mean':
                        
                        #Get properly formatted precision on labels of mean +/- sd
                        mean = np.array(df)
                        if error_type == 'sd':
                            error = heatmaps[heatmap_key]['sd']
                        elif error_type == 'se':
                            error = heatmaps[heatmap_key]['se']
                        labels = AdaptiveConfidenceBCM.print_avg_with_std(mean, error)
                        
#                         mean_str = df.round(decimal_places).astype(str)

#                         joiner = mean_str.copy()
#                         for col in joiner.columns:
#                             joiner[col].values[:] = u"\u00B1"# " +/- "

#                         sd_str = pd.DataFrame(heatmaps[heatmap_key]['sd'], index = cs, columns = mus)
#                         sd_str = sd_str.round(decimal_places).astype(str)

#                         labels = mean_str + joiner + sd_str
#                         labels = labels.to_numpy(dtype=str)

                        #Plot heat map with labels of average +/- sd
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, 
                                    annot = labels, fmt = "", annot_kws={"size":fontsizes['XS']})

                    else:
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = True,
                                    fmt="0."+str(decimal_places)+"f", annot_kws={"size":fontsizes['XS']})
                else:
                    g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = False)
                #ax = sns.heatmap(df, cmap = "rainbow", vmin = vmin, vmax = vmax)

                # #Set subplot axis labels
                # if j == 0:
                #     ax.set_ylabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['M'])
                # if i == len(deltas) - 1:
                #     ax.set_xlabel(r"Compromise parameter ($\mu$)", fontsize = fontsizes['M'])

                #Set subplot title
                title = r"$\delta$ = " + str(delta) + r", $\gamma$ = " + str(gamma) 
                ax.set_title(title, fontsize = fontsizes['L'])

                #Set tick label size
                ax.set_xticklabels(mus, size = fontsizes['S'])
                ax.set_yticklabels(cs, rotation = 0, size = fontsizes['S'])
         
        # ----------------------------------------------------
        #Separately plot the heatmap for the control "baseline DW" model case of delta = 1.0, gamma = 0.0
        delta, gamma = 1.0, 0.0
        heatmap_key = 'delta=' + str(delta) + ', gamma=' + str(gamma) #Get the heatmap key

        #single out the right axis
        if len(deltas) == 1:
            row = 0
            ax = axs[0] #axs[len(gammas)]
        else:
            row = 1
            ax = axs[row, 0] #axs[row, len(gammas)] #single out the right axis
            for i in range(0, len(deltas)): #turn off the extra plots
                if i != row:
                    axs[i, 0].axis('off')
                    #axs[i, len(gammas)].axis('off')

        matrix = plot_maps[heatmap_key] #get the matrix for the heatmap

        #Convert matrix to dataframe for axis labels
        df = pd.DataFrame(matrix, index = cs, columns = mus)

        #Plot heat map with or without entry values depending on show_values
        if show_labels:
            #Get the annotated values of average +/- sd
            if print_error and stat == 'mean':
                
                #Get properly formatted precision on labels of mean +/- sd
                mean = np.array(df)
                sd = heatmaps[heatmap_key]['sd']
                labels = AdaptiveConfidenceBCM.print_avg_with_std(mean, sd)

                #Plot heat map with labels of average +/- sd
                g = sns.heatmap(df, ax = ax, 
                            cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                            vmin = vmin, vmax = vmax, 
                            annot = labels, fmt = "", annot_kws={"size":fontsizes['XS']})

            else:
                g = sns.heatmap(df, ax = ax, 
                            cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                            vmin = vmin, vmax = vmax, annot = True,
                            fmt="0."+str(decimal_places)+"f", annot_kws={"size":fontsizes['XS']})
        else:
            g = sns.heatmap(df, ax = ax, 
                            cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                            vmin = vmin, vmax = vmax, annot = False)
        #ax = sns.heatmap(df, cmap = "rainbow", vmin = vmin, vmax = vmax)

        #Set subplot axis labels
        # ax.set_ylabel("Initial confidence bound ($c_0$)", fontsize = fontsizes['L'])
        # ax.set_xlabel("Compromise parameter ($\mu$)", fontsize = fontsizes['L'])

        #Set subplot title
        title = r"$\delta$ = " + str(delta) + r", $\gamma$ = " + str(gamma) 
        ax.set_title(title, fontsize = fontsizes['L'])

        #Set tick label size
        ax.set_xticklabels(mus, size = fontsizes['S'])
        ax.set_yticklabels(cs, rotation = 0, size = fontsizes['S'])
    
        # ----------------------------------------------------
        #Set colorbar and tick label font size
        cbar_ax.tick_params(labelsize=fontsizes['M'])
        cbar_ax.set_xlabel(title_prefix[stat] + plot_title[key], fontsize=fontsizes['XL'])
        #cbar_ax.tick_params(axis='both', labelsize=fontsizes['M'])
        
        #Large shared axis labels
        fig.supylabel("Initial confidence bound ($c_0$)", fontsize = fontsizes['XL'])
        fig.supxlabel(r"  Compromise parameter ($\mu$)", fontsize = fontsizes['XL'])
    
        #Large supertitle
        # suptitle = title_prefix[stat] + plot_title[key] + " for Complete(N=" + str(n) + ")"
        # plt.suptitle(suptitle, fontsize = fontsizes['XL'])
        
        #Save the plot
        savefile = directory + "/"
        if (not network_dataset) and (n == 100) and (graph_type == "complete") and (full_grid == True):
            savefile = savefile + "full_grid/"
        if print_error and error_type == "se":
            savefile = savefile + "with_se/"
        if not os.path.exists(savefile):
            os.makedirs(savefile)
        if network_dataset:
            savefile = savefile + f"{graph_name}--"
        else:
            savefile = savefile + f"{graph_type}{n}--"
        savefile = savefile + plot_name[key] + save_suffix[stat]
        savefile = savefile + ('--labeled' * show_labels)
        if print_error and stat == 'mean':
            savefile = f"{savefile}--with_{error_type}"
        savefile = savefile + ".png"
        plt.savefig(savefile, bbox_inches='tight', facecolor='white')
        plt.show()
        