'''
Author: Grace Li
Date: 11/23/2021

This script calculates the results from our simulations of our adaptive-confidence DW model. Run this script after
running simulations using either completeDW.py or netscienceDW.py

After running this script, the linePlots OR heatmap scripts can be used to generate plots of our simulation results.

To run this script, at the top, modifiy the variables that give the desired graph, number of nodes, and model parameters 
confidence-increase parameter delta, confidence-decerase parameter gamma, compromise parameter mu, and 
initial confidence bound c - this is called c0 in the paper)

For each simulation, the
    - confidence-increase parameter gamma
    - confidence-decrease parameter delta
    - compromise parameter mu
    - initial confidence bound c (called c0 in the paper)
    - opinion set (abbreviated to op in the code), 
are used to load the stored matfile for that simulation.

The following quantities are calculated at convergence time for each simulation:
    - number of opinion clusters
    - number of major opinion clusters
    - number of minor opinion clusters
    - maximum pairwise difference in opinion of nodes in the same opinion cluster (we call this the opinion diameter of the cluster)
    - convergence time (as a number of time steps)
    - whether or not the bailout time is reached (as a boolean)
    - mean local receptiveness
    - Shannon entropy H
    - mean, variance, skew and kurtosis of opinions
    - fraction of edges in the effective graph (out of edges in the original graph)
    - weighted average of the fraction of edges in the effective graph (W(T))
    - fraction of edges deleted in the effective graph
    - fraction of within-opinion-cluster edges deleted out of total deleted edges in the effective graph
We store these calculated results as a row in a csv file. For each (gamma, delta) pair, we generate and save a csv file
of simulation results and store it in the "combined_results" folder for that graph. 

IMPORTANT: This script requires igraph version 0.9.7 as the syntax of that version is used to calculate our various quantities.

'''

# import required packages  

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

minor_clusters_percent = 1

## Change parameters here ---------------------------------------------------------------

#name of experiment folder
graph_name = "netscience" #If we have a specific graph
network_dataset = True #Set equal to true if we want to use graph name

graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 100

runtoTmax = False
toltest = False
tol = 0.001 #specify the tolerance value if its not the standard 0.02

max_times_continued = 1 #Maximum times we continued the simulation so we check each folder

#savefolder name for experiment
if network_dataset:
    experiment = 'NetworkDatasets/' + graph_name + '_results' + ("/runtoTmax" * runtoTmax)
else:
    experiment = graph_type.capitalize() + '/' + graph_type + str(n) + ("/runtoTmax" * runtoTmax)
    
#Specify parameters

zoom_in = False

#Confidence-increase parameter
gammas = [0.1, 0.3]

#Confidence-decrease parameter
deltas = [0.5]

if (not network_dataset) and (n == 100) and (graph_type == "complete"):
    gammas = [0.1, 0.3, 0.5] #Confidence-increase parameter
    deltas = [0.3, 0.5, 0.7] #Confidence-decrease parameter

delta_gammas = [(1.0, 0.0)]

for delta in deltas:
    for gamma in gammas:
        delta_gammas.append((delta, gamma))

#Initial confidence bound
cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
zoom_cs = []
if zoom_in:
    zoom_cs = [0.1, 0.15, 0.2]
    zoom_cs = [0.1, 0.125, 0.15, 0.175, 0.2]
    cs = set(cs + zoom_cs)
    cs = list(cs)
# cs = [0.2, 0.3, 0.5, 0.7, 0.9]

#Compromise parameter
mus = [0.1, 0.3, 0.5]
zoom_mus = [0.1, 0.2, 0.3, 0.4, 0.5]

#specify opinions sets
opinion_sets = list(range(10))
n_opinion_sets = len(opinion_sets)


def generate_row(results, minor_threshold, graph):
    '''
    Given the resulting output from opening a stored matfile, put together the row entry
    of the Pandas DataFrame to store the results
    
    minor_threhold: int,
        For cluster sizes <= minor threshold, we consider them minor clusters, otherwise we consider them major clusters
    graph: igraph graph
    '''
    
    #Calculate the average local receptiveness
    avg_local_receptiveness = np.mean(results['local_receptiveness'][0])
        
    #Calculate the Shannon entropy for the normalized clusters
    n_clusters = results['n_clusters'][0][0]
    n_minor = 0 #start counting the number of minor clusters
    #If we reached the bailout time, then there's no clusters found and we set everything to NaN
    if n_clusters == 0:
        n_clusters = np.nan
        n_minor = np.nan
        n_major = np.nan
        H = np.nan
        max_diameter = np.nan
    #If we sucessfully determined the clusters, then we calculate the Shannon entropy and max cluster diameter
    else:
        diameters = []
        cluster_sizes = [] #get and store the size of each opinion cluster
        for i in range(n_clusters):
            key = "cluster" + str(i)
            cluster = results[key][0]
            cluster_sizes.append(len(cluster))
            if len(cluster) <= minor_threshold:
                n_minor += 1
            final_ops = results['final_opinions'][0][cluster]
            diameters.append(max(final_ops) - min(final_ops)) 
        cluster_sizes = np.array(cluster_sizes) / np.sum(np.array(cluster_sizes)) #normalize the cluster sizes
        #Calculate Shannon's entropy of normalized clusters
        H = - np.sum(cluster_sizes * np.log(cluster_sizes))
    
        n_major = n_clusters - n_minor
    
        max_diameter = max(diameters)
        
    #Calculate the mean, variance, skewness, and kurtosis of the final opinion vector
    final_opinions = results['final_opinions'][0]
    op_mean = np.mean(final_opinions)
    op_var = np.var(final_opinions)
    op_skew = stats.skew(final_opinions, axis=None)
    op_kurtosis = stats.kurtosis(final_opinions, axis=None, fisher=True)
    
    #Reconstruct the effective graph at T and look at the edges remaining
    G = graph.copy()
    n = G.vcount()
    edge_start = np.array([e.tuple[0] for e in G.es], dtype=np.int64)
    edge_end = np.array([e.tuple[1] for e in G.es], dtype=np.int64)
    
    confidence = results['confidence'][0]
    G.vs['opinion'] = final_opinions
    opinion_diff = np.array(G.vs[edge_start.tolist()]['opinion']) - np.array(G.vs[edge_end.tolist()]['opinion'])
    opinion_diff = np.abs(opinion_diff)
    
    #Get the edges which have opinion difference < d and convert them to an edge list
    index = np.argwhere(opinion_diff < confidence)
    index = index.flatten()
    sub_edge_start = edge_start[index]
    sub_edge_end = edge_end[index]
    edge_list = [(sub_edge_start[i], sub_edge_end[i]) for i in range(len(index))]

    #Construct an effective subgraph subG consisting of these edges with possible interactions 
    subG = igraph.Graph()
    subG.add_vertices(G.vcount())
    subG.add_edges(edge_list)
    
    #1. Fraction of edges remaining overall
    frac_edges = len(subG.es) / len(G.es)
    
    if np.isnan(n_clusters):
        wt_avg_frac_edges_cluster = np.nan
        deleted_within_of_total = np.nan
        deleted_within_of_deleted = np.nan

    else:
        #2. Weighted sum of fraction edges per cluster remaining
        n_singletons = 0
        wt_avg_frac_edges_cluster = 0
        subG_within_cluster_edges = 0
        G_within_cluster_edges = 0
        for i in range(n_clusters):
            key = "cluster" + str(i)
            cluster = results[key][0]
            G_cluster = G.subgraph(cluster)
            subG_cluster = subG.subgraph(cluster)
            subG_within_cluster_edges += len(subG_cluster.es)
            G_within_cluster_edges += len(G_cluster.es)
            if len(G_cluster.es) > 0:
                wt_avg_frac_edges_cluster += len(cluster) * len(subG_cluster.es) / len(G_cluster.es)
            else:
                n_singletons += 1
        wt_avg_frac_edges_cluster = wt_avg_frac_edges_cluster / (n - n_singletons)

        #3. Deleted within cluster edges / total original edges
        #4. Deleted within cluster edges / total deleted edges
        deleted_within = G_within_cluster_edges - subG_within_cluster_edges
        deleted_total = len(G.es) - len(subG.es)

        deleted_within_of_total = deleted_within / len(G.es)
        if deleted_total > 0:
            deleted_within_of_deleted = deleted_within / deleted_total
        else:
            deleted_within_of_deleted = np.nan
    
    #Assemble the results into a dataframe row
    columns = ['delta', 'gamma', 'c', 'mu', 'opinion_set',
               'n_clusters', 'n_major', 'n_minor', 'max_diameter', 
               'T', 'T_changed', 'bailout', 
               'avg_opinion_diff', 'avg_local_receptiveness', 'entropy',
               'op_mean', 'op_var', 'op_skew', 'op_kurtosis',
               'frac_edges', 'wt_avg_frac_edges_cluster',
               'deleted_within_of_total', 'deleted_within_of_deleted'
              ]
    row = pd.DataFrame(columns = columns)
    row.loc[0] = [delta, gamma, c, mu, opinion_set,
                  n_clusters, n_major, n_minor, max_diameter,
                  results['T'][0][0], results['T_changed'][0][0],
                  results['bailout'][0][0],
                  results['avg_opinion_diff'][0][0],
                  avg_local_receptiveness, H,
                  op_mean, op_var, op_skew, op_kurtosis,
                  frac_edges, wt_avg_frac_edges_cluster,
                  deleted_within_of_total, deleted_within_of_deleted
                 ]
    
    return row


#Read the stored matfiles and create a pandas dataframe of simulation results for each run
#including number of clusters, convergence time, number of positive interactions (T_changed), 
#and average opinion distance

#Specify column names for the storage dataframe
columns = ['delta', 'gamma', 'c', 'opinion_set',
           'n_clusters', 'n_major', 'n_minor',
           'max_diameter', 'T', 'bailout', 
           'avg_opinion_diff', 'avg_local_receptiveness', 'entropy',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis',
           'frac_edges', 'wt_avg_frac_edges_cluster',
           'deleted_within_of_total', 'deleted_within_of_deleted'
          ]

total_bailout = 0
total_continued = 0


with open(experiment + '/MatfileConsolidatorOutputs.txt', 'w') as f:
    
    if not network_dataset:
        minor_threshold = math.floor(n * minor_clusters_percent/100)  
        print('\nn = ', n, file=f, flush=True)
        print('The minor threshold is: ', minor_threshold, file=f, flush=True)

    if not network_dataset and graph_type == "complete":
        G = igraph.Graph.Full(n)
        
    if network_dataset:
        filename = f"NetworkDatasets/{graph_name}_data/{graph_name}.gml"
        G = igraph.Graph.Read_GML(filename)
        print(graph_name)
        print('Original number of vertices:', G.vcount())

        component_membership = G.clusters(mode='strong').membership
        sizes = G.clusters(mode='strong').sizes()
        index_max = sizes.index(max(sizes))
        component = np.nonzero(np.array(component_membership)==index_max)[0]
        G = G.induced_subgraph(component)
        print('GCC number of vertices:', G.vcount())

        #Get the graph size and minor cluster threshold
        n = G.vcount()
        print('\nn = ', n, file=f, flush=True)
        #For cluster sizes <= minor threshold, we consider them minor clusters. Otherwise we consider them major clusters
        minor_threshold = math.floor(n * minor_clusters_percent/100)
        # minor_threshold = math.ceil(n * minor_clusters_percent/100)
        print('The minor threshold is: ', minor_threshold, file=f, flush=True)
        
    #Fill in the data frame row by row and store
    #We will create a new dataframe for each delta, gamma pair
    for pair in delta_gammas:
        delta = pair[0]
        gamma = pair[1]

        delta_gamma_bailout = 0
        print('\ndelta =', delta, ' and gamma = ', gamma, file=f, flush=True)
        print('\ndelta =', delta, ' and gamma = ', gamma)

        #Initialize the dataframe
        df = pd.DataFrame(columns = columns)

        for c in sorted(set(cs + zoom_cs)):
            all_mus = mus
            if c in zoom_cs:
                all_mus = zoom_mus
            for mu in all_mus:
                print('c =', c, ' and mu = ', mu, file=f, flush=True)
                bailout_per_point = 0
                
                # print('c =', c, ' and mu = ', mu)
                for opinion_set in opinion_sets:

                    # for i in list(range(max_times_continued, 0, -1)): #range(start, stop, step)
                    #     try: 
                    #         matfile = (experiment + '/matfiles/delta' 
                    #                + str(delta) + '-gamma' + str(gamma) + '/continue' + str(i) + '/'
                    #                + 'delta' + str(delta) + '-gamma' + str(gamma) 
                    #                + '--c' + str(c) + '-mu' + str(mu)
                    #                + '-op' + str(opinion_set) + '.mat')
                    #         results = io.loadmat(matfile)
                    #         print('We continued the simulation', i, ' times for ', matfile, file=f, flush=True)
                    #         total_continued += 1
                    #         break
                    #     except:
                    #         if i == 1:
                    #Default name for matfile if we didn't continue the simulation
                    matfile = (experiment + '/matfiles/delta' 
                       + str(delta) + '-gamma' + str(gamma) + '/'
                       + 'delta' + str(delta) + '-gamma' + str(gamma) 
                       + '--c' + str(c) + '-mu' + str(mu)
                       + '-op' + str(opinion_set) + '.mat')

                    try:
                        results = io.loadmat(matfile)

                        #Parse the results into a dataframe row and add to df
                        row = generate_row(results, minor_threshold, G)
                        df = df.append(row, ignore_index=True)

                        #We didn't use continue files for DW on complete100 with gamma = 0.1, delta = 0.5
                        if not network_dataset and gamma == 0.1 and delta == 0.5 and row['T'][0] > 1e6:
                            print('Bailout time reached for ' +  matfile, file=f, flush=True)
                            print('Time was ' + str(results['T'][0][0]), file=f, flush=True)
                            bailout_per_point += 1
                            delta_gamma_bailout += 1
                        
                        elif row['bailout'][0]:
                            print('Bailout time reached for ' +  matfile, file=f, flush=True)
                            print('Bailout time was ' + str(results['T'][0][0]), file=f, flush=True)
                            print('Max opinion cluster diameter: ', row['max_diameter'][0], file=f, flush=True)
                            delta_gamma_bailout += 1
                            bailout_per_point += 1

                    except:
                        print("file does not exist: ", matfile, file=f, flush=True)
                        print("file does not exist: ", matfile)
                
                if bailout_per_point > 0:        
                    print('c =', c, ' and mu = ', mu, '- Number of bailouts =', bailout_per_point,  file=f, flush=True)

        #Make sure the opinion_set, n_clusters, and time steps are integers instead of floats
        df = df.astype({'opinion_set': int, 'T': int, 'T_changed': int})

        #Check that a directory exists, and if not, create it
        directory = experiment + '/combined_results/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        filename = experiment + '/combined_results/delta' + str(delta) 
        filename = filename + '-gamma' + str(gamma) 
        filename = filename + (("--tol" + str(tol)) * toltest) + '.csv'
        df.to_csv(filename, index=False, header=True)
        
        print(f'Total bailout reached for gamma = {gamma}, delta = {delta}: {delta_gamma_bailout}', file=f, flush=True)
        print(f'Total bailout reached for gamma = {gamma}, delta = {delta}: {delta_gamma_bailout}')

    print('Total simulations sucessfully continued:', total_continued, file=f, flush=True)
    print('Total bailout reached to rerun:', total_bailout, file=f, flush=True)