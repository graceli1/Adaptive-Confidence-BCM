'''
Author: Grace Li
Date: 5/11/2022

This script generates plots of the results of the simulations for our adaptive-confidence DW BCM.
In the paper, we examine the numbers of major and minor clusters, Shannon entropy, W(T), and convergence time. 
Here, we also plot the variance of final opinions, mean local receptiveness, fraction of edges in the effective graph. 
and the fraction of deleted edges overall in the effective graph and within clusters of the effective graph.

For each quantity of interest, this script generates a single figure where each plot shows the quantity of interest
versus the initial confidence bound c0 (called c in the code). It generates figures where the plots are all in a single row.
Each plot represents a single confidence-increase and confidence-decrease parameter pair (gamma, delta). Each plot has different 
colored curves corresponding to different delta values. We generate each point on the plot from 10 numerical simulations 
each with different sets of initial opinions drawn uniformly at random. We plot the mean with error range representing 
one standard deviation across the numerical simulations for that point.

''' 

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own adaptive-confidence bounded-confidence module
import sys
sys.path.append('..') #look one directory above
import AdaptiveConfidenceBCM

sns.set_style("ticks",{'axes.grid' : True})

#Change experiment parameters here -----------------------------------------------

#name of experiment folder
graph_name = "netscience" #If we have a specific graph
network_dataset = False #Set equal to true if we want to use graph name

graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 100

#se for standard error, sd for standard deviation
error_type = "sd" 

runtoTmax = False
toltest = False
tol = 0.01 #specify the tolerance value if its not the standard 0.02

#Specify which quantities to plot
keys = [
        'log(T)', 'entropy',
        'n_major', 'n_minor',
        'wt_avg_frac_edges_cluster'
       ]
# keys = [
#         'log(T)',
#         'n_clusters', 'entropy',
#         'avg_local_receptiveness',
#         'n_major', 'n_minor',
#         'op_var',
#         'frac_edges', 'wt_avg_frac_edges_cluster',
#         'deleted_within_of_total', 'deleted_within_of_deleted'
#        ]
    
#(gamma, delta) combinations
#gamma is the confidence-increase parameter, delta is the confidence-decrease parameter
gamma_delta_pairs = [(0.0, 1.0), (0.1, 0.5), (0.3, 0.5), (0.5, 0.5)]
if network_dataset:
    gamma_delta_pairs = [(0.0, 1.0), (0.1, 0.5), (0.3, 0.5)]
    if graph_name == "karate":
            gamma_delta_pairs = [(0.0, 1.0), (0.1, 0.5)]
elif n != 100:
    gamma_delta_pairs = [(0.0, 1.0), (0.1, 0.5)]
    
# gamma_delta_pairs = [(0.0, 1.0), (0.1, 0.5), (0.3, 0.5)] #jerry

#Initial confidence bound
cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
xtick_list = [0.1, 0.3, 0.5, 0.7, 0.9]

#Compromise parameter
mus = [0.1, 0.3, 0.5]

#savefolder name for experiment
if network_dataset:
    experiment = 'NetworkDatasets/' + graph_name + '_results' + ("/runtoTmax" * runtoTmax)
else:
    experiment = graph_type.capitalize() + '/' + graph_type + str(n) + ("/runtoTmax" * runtoTmax)

#Plot parameters  ---------------------------------------------------------------------------
#Plot font size parameters
# fontsizes = {'XS': 10, 'S': 15, 'M': 24, 'L':30, 'XL': 35, 'space': 20}
# fontsizes = {'XS': 13, 'S': 16, 'M': 24, 'L':25, 'XL': 32, 'XXL':45, 'space':35, 'left_space': 20}
fontsizes = {'XS': 13, 'S': 16, 'M': 24, 'L':25, 'XL': 35, 'XXL':40, 'space':30, 'left_space': 20} 

# color_palette = ['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#ffff33','#a65628']
# color_palette = sns.color_palette("viridis", as_cmap=True)
colorlist = ['#0173b2', '#de8f05', '#029e73']
markerlist = ["o", "s", "^"]


plot_height = 5.8
plot_width = 5
if not network_dataset:
    plot_width = 5.3

rows, cols = 1, len(gamma_delta_pairs)
figsize = (plot_width*cols, plot_height*rows)
bbox_to_anchor=(1.0, 0.5)
    
plot_name = {
                'log(T)': 'T',
                'n_clusters': 'clusters', 
                'n_minor': 'n_minor',
                'n_major': 'n_major',
                'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis',
                'frac_edges': 'frac_edges', 
                'wt_avg_frac_edges_cluster': 'wt_avg_frac_edges',
                'deleted_within_of_total': 'deleted_within_of_total',
                'deleted_within_of_deleted': 'deleted_within_of_deleted'
            }
plot_ylabel = {
                'log(T)': r"$\log_{10}(T_f)$", 
                'n_clusters': 'Number of clusters',
                'n_minor': 'Number of\nminor clusters',
                'n_major': 'Number of\nmajor clusters',
                'entropy': 'Shannon entropy\n' + "$H(T_f)$",
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis',
                'frac_edges': 'Fraction of edges', 
                'wt_avg_frac_edges_cluster': r"$W(T_f)$",
                'deleted_within_of_total': 'Fraction of\ndisconnected edges',
                'deleted_within_of_deleted': 'Fraction of within-cluster\ndisconnected edges'
             }


plot_title = plot_ylabel.copy()
plot_title['deleted_within_of_total'] = 'Fraction of disconnected edges'
plot_title['deleted_within_of_deleted'] = 'Fraction of within-cluster disconnected edges'


#Names for the columns of the consolidated matfiles
columns = ['delta', 'gamma', 'c', 'opinion_set',
           'n_clusters', 'n_major', 'n_minor',
           'max_diameter', 'T', 'bailout', 
           'avg_opinion_diff', 'avg_local_receptiveness', 'entropy',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis',
           'frac_edges', 'wt_avg_frac_edges_cluster',
           'deleted_within_of_total', 'deleted_within_of_deleted'
          ]

idx_to_letter = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H', 8:'I', 9:'J', 10:'K'}


# Plot results for each gamma/delta pair ---------------------------------------------------

#Check that a directory exists, and if not, create it
directory = experiment + f"/combined_plots/"
if not os.path.exists(directory):
    os.makedirs(directory)

#Loop through the quantities of interest
for key in keys:
    print(key)

    #Get min and max for the vertical axis
    min_y, max_y = 999999999999, 0
    for i in range(len(gamma_delta_pairs)):
        pair = gamma_delta_pairs[i]
        gamma, delta = pair[0], pair[1]

        #Load the simulation results stored csv files (from MatfileConsolidator.py)
        filename = experiment + '/combined_results/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
        sim_results = pd.read_csv(filename)

        sim_results = sim_results[sim_results['mu'].isin(mus)]

        #Make sure the opinion_set, and time steps are integers instead of floats
        sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'T_changed': int})
        #Calculate the log of time steps to make it easier to visualize
        sim_results['log(T)'] = np.log10(sim_results['T'])
        sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

        #Update max an min values
        min_y = min(min_y, sim_results[key].min())
        max_y = max(max_y, sim_results[key].max())

    #Create the plot
    # fig, axs = plt.subplots(rows, cols constrained_layout=True, figsize=figsize)
    fig, axs = plt.subplots(rows, cols, figsize=figsize)
    
    label_idx = -1

    #Generate each subplot
    for row in range(rows):
        for col in range(cols):
            
            label_idx += 1

            ax = axs[col]
            pair = gamma_delta_pairs[col]
            gamma, delta = pair[0], pair[1]

            #Load the simulation results stored csv files (from MatfileConsolidator.py)
            filename = experiment + '/combined_results/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
            sim_results = pd.read_csv(filename)

            sim_results = sim_results[sim_results['mu'].isin(mus)]
            
            #Make sure the opinion_set, and time steps are integers instead of floats
            sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'T_changed': int})

            #Calculate the log of time steps to make it easier to visualize
            sim_results['log(T)'] = np.log10(sim_results['T'])
            sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])
            
            #Convert delta to a string so the legend displays properly
            sim_results['delta'] = str(delta)
            if delta == 1.0:
                sim_results['delta'] = '1'

            #Generate pointplot for visualization for this delta value
            # sns.pointplot(x='c', y=key, ax = ax, data=sim_results)
            sns.lineplot(x ='c', y = key, ax = ax, data = sim_results, errorbar=error_type,
                             hue = "mu",  palette = colorlist, 
                             style = "mu", markers = markerlist, dashes = False, markersize=10)            

            if gamma == 0.0 and delta == 1.0:
                title = "Baseline DW model\n" + r"($\gamma = 0$, $\delta = 1$)"
            else:            
                title = r"$\gamma = $" + str(gamma) +  r"$, \delta = $" + str(delta)
            ax.set_title(title, fontsize = fontsizes['L'], pad = fontsizes['XS'])

            # if col == 0 and row == 0:
            #     ax.set_ylabel(plot_ylabel[key], fontsize = fontsizes['XL'], va='center', labelpad = fontsizes["left_space"])
            if col == 0:
                ax.set_ylabel(" ", fontsize = fontsizes['left_space'])
                ax.yaxis.label.set_color('white')   
            else:
                ax.set(ylabel=None)
            
            if network_dataset and col == 1 and cols == 3:
                ax.set_xlabel(r"Initial confidence bound ($c_0$)", fontsize = fontsizes['XL'], labelpad = fontsizes["space"])
            else:
                ax.set_xlabel(" ", fontsize = fontsizes["space"])
                ax.xaxis.label.set_color('white')
                ax.set(xlabel=None)
                
            ax.set_xticks(xtick_list)
            ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
            
            #y ticks
            if key in ['frac_edges', 'wt_avg_frac_edges_cluster', 'deleted_within_of_total', 'deleted_within_of_deleted', 'avg_local_receptiveness']:
                ax.set_ylim(-0.05, 1.05)
                ax.set_yticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
            elif key == "entropy":
                ax.set_ylim(-0.1, AdaptiveConfidenceBCM.round_decimals_up(max_y, 1))
            elif key == "n_major":
                if network_dataset and graph_name == "netscience":
                    ax.set_ylim(0, 25)
                else:
                    ax.set_ylim(0, AdaptiveConfidenceBCM.round_decimals_up(max_y, 1))
            elif key in ["n_minor", 'n_clusters']:
                max_value = AdaptiveConfidenceBCM.round_decimals_up(max_y, 1)
                if max_value < 10:
                    ax.set_ylim(-0.1, max_value)
                elif max_value < 100:
                    ax.set_ylim(-1, max_value)
                else:
                    ax.set_ylim(-5, max_value)
            elif key == ['op_var']:
                ax.set_ylim(-0.02, AdaptiveConfidenceBCM.round_decimals_up(max_y, 1))
            else:
                ax.set_ylim(AdaptiveConfidenceBCM.round_decimals_down(min_y, 1), AdaptiveConfidenceBCM.round_decimals_up(max_y, 1))
            

            #Hide the legend
            ax.get_legend().remove()

            #For the first gamma plot we generate, create a global legend in the last corner
            if row == 0 and col == 0:
                legend = fig.legend(title = r"$\mu$", loc='center left', bbox_to_anchor=bbox_to_anchor,
                          fontsize = fontsizes['M'], title_fontsize = fontsizes['L'], markerscale=2)
                
            ax.text(-0.08, 1.2, idx_to_letter[label_idx], transform=ax.transAxes, fontsize=fontsizes['XL'], fontweight='bold', va='top', ha='right')
            

    fig.supylabel(plot_ylabel[key], fontsize = fontsizes['XXL'], va='center', ha='center')
    if (not network_dataset) or (cols < 3):
        fig.supxlabel(" "*8 + r"Initial confidence bound ($c_0$)", fontsize = fontsizes['XXL'])
    
    #Save the plot
    savefile = directory + "/"
    if error_type == "se":
        savefile = savefile + "with_se/"
    if not os.path.exists(savefile):
        os.makedirs(savefile)
    if network_dataset:
        savefile = savefile + f"{graph_name}--"
    else:
        savefile = savefile + f"{graph_type}{n}--"
    savefile = savefile + plot_name[key] + ".png"
    # savefile = savefile + plot_name[key] + "-jerry.png" #jerry
    
    fig.tight_layout(w_pad=4, h_pad=3)
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    plt.show()