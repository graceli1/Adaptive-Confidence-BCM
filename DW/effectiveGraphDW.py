'''
Author: Grace Li
Date: 12/19/2021

This script generates visualizations of the effective graph at convergence time for our adpative-confidence DW simulations.

For the graph visualizations, one can select whether to color nodes by initial or final opinion (using node_color_type) and 
whether to color edges black or by confidence bound (using edge_color_type). The include_cbar variable determines whether
the corresponding color bars are shown for the node/edge colors. 

For each simulation, this script outputs the visualization of the entire effective graph if it is not the same as the
original graph. If there are multiple opinion clusters, then each major opinion cluster is also visualized separately. 
The resulting visualizations are stored in the "effective_graphs" folder of the corresponding graph type/name.

IMPORTANT: This script requires igraph version 0.9.7. The syntax of that version is used for the visualizations.

'''

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
# from matplotlib import animation
# import seaborn as sns
import time as time
import igraph as igraph
import os.path
# from os import getpid
from matplotlib.artist import Artist
from igraph import BoundingBox, Graph, palettes

# # Import our own adaptive-confidence BCM module
# import sys
# sys.path.append('..') #look one directory above
# sys.path.append('../..') #look two directories above
# import AdaptiveConfidenceBCM

minor_clusters_percent = 1

## EXPERIMENT DETAILS

# #name of experiment folder
graph_type = "complete" #If we are generating a synthetic network with n nodes
n = 100

graph_name = "netscience" #If we have a specific graph
network_dataset = False #Set equal to true if we want to use graph name
if network_dataset:
    graph_type = "dataset"

runtoTmax = False

#whether to color nodes by initial or final opinion
node_color_type = "initial_opinion"

#whether to color edges black or by confidence bound
edge_color_type = "black"

#Whether or not to include a colorbar
include_cbar = True

#Whether or not to include information in the title
show_title = False

gamma = 0.1 #Confidence-increase parameter
delta = 0.5 #Confidence-decrease parameter

cs = [0.1, 0.2] #Initial confidence bound
mus = [0.1, 0.3, 0.5] #Compromise parameter

opinion_sets = list(range(10))

opinion_sets = [6,2]
cs = [0.1]
mus = [0.1]

fontsizes = {'S':22, 'M':30, 'L':30}

## ---------------------------------------

#colorbar colormaps
opinion_cmap = plt.cm.rainbow
opinion_norm = mpl.colors.Normalize(vmin=0, vmax=1)
cmap = plt.cm.inferno_r #rainbow_r

# Make Matplotlib use a Cairo backend
mpl.use("cairo")

#Set the folder to get the simulation data from and make sure plot folder exists
if not network_dataset and graph_type == "complete":
    experiment = f"Complete/complete{n}"
elif network_dataset and graph_name == "netscience":
    experiment = f"NetworkDatasets/netscience_results"
if runtoTmax:
    experiment = experiment + "/runtoTmax"
    
directory = experiment
directory = directory + f"/effective_graphs/delta{delta}-gamma{gamma}"
if not os.path.exists(directory):
    os.makedirs(directory)
    
#Class to handle the igraph plot and generate it as desired
class GraphArtist(Artist):
    """Matplotlib artist class that draws igraph graphs.
    Only Cairo-based backends are supported.
    """

    def __init__(self, graph, bbox, palette=None, *args, **kwds):
        """Constructs a graph artist that draws the given graph within
        the given bounding box.

        `graph` must be an instance of `igraph.Graph`.
        `bbox` must either be an instance of `igraph.drawing.BoundingBox`
        or a 4-tuple (`left`, `top`, `width`, `height`). The tuple
        will be passed on to the constructor of `BoundingBox`.
        `palette` is an igraph palette that is used to transform
        numeric color IDs to RGB values. If `None`, a default grayscale
        palette is used from igraph.

        All the remaining positional and keyword arguments are passed
        on intact to `igraph.Graph.__plot__`.
        """
        Artist.__init__(self)

        if not isinstance(graph, Graph):
            raise TypeError("expected igraph.Graph, got %r" % type(graph))

        self.graph = graph
        self.palette = palette or palettes["gray"]
        self.bbox = BoundingBox(bbox)
        self.args = args
        self.kwds = kwds

    def draw(self, renderer):
        from matplotlib.backends.backend_cairo import RendererCairo
        if not isinstance(renderer, RendererCairo):
            raise TypeError("graph plotting is supported only on Cairo backends")
        self.graph.__plot__(renderer.gc.ctx, self.bbox, self.palette, *self.args, **self.kwds)


#Initialize original graph
if network_dataset:
    if graph_name == "netscience":
        #Read the netscience network and get the largest connected component
        G = igraph.Graph.Read_GML('NetworkDatasets/netscience_data/netscience.gml')

        component_membership = G.clusters(mode='strong').membership
        sizes = G.clusters(mode='strong').sizes()
        index_max = sizes.index(max(sizes))
        component = np.nonzero(np.array(component_membership)==index_max)[0]

        G_original = G.induced_subgraph(component)
        n = len(G.vs)
        print('GCC')
        print('Number of vertices:', n)
        print('Number of edges:', len(G.es))
        
elif graph_type == "complete":
    G_original = igraph.Graph.Full(n)
        
    
        
#Loop through the parameters and generate the effective subgraph plot
for c in cs:
    for mu in mus:
        for opinion_set in opinion_sets:
            
            continue_past_bailout = False
            file_found = False
            
            #Load the matfile for this experiment
            try:
                matfile = (
                            f"{experiment}/matfiles/delta{delta}-gamma{gamma}/continue/"
                            f"delta{delta}-gamma{gamma}--c{c}-mu{mu}-op{opinion_set}.mat"
                          )
                results = io.loadmat(matfile)
                print('We continued the simulation for ', matfile)
                continue_past_bailout = True
                file_found = True
            except:
                matfile = (
                            f"{experiment}/matfiles/delta{delta}-gamma{gamma}/"
                            f"delta{delta}-gamma{gamma}--c{c}-mu{mu}-op{opinion_set}.mat"
                          )
                results = io.loadmat(matfile)
                file_found = True
                
            if file_found:
                #Reinitialize graph
                G = G_original.copy()

                G.vs['opinion'] = results['final_opinions'][0] #assign final opinions
                avg_opinion = sum(G.vs['opinion']) / len(G.vs['opinion'])
            
            
                ## Get the edge information for the graph
                # Store the start and end nodes for each edge (igraph will return edge_start < edge_end for each edge)
                # and the magnitude of the opinion difference and confidence bound on that edge as numpy arrays
                # Edge start and end
                edge_start = np.array([e.tuple[0] for e in G.es], dtype=np.int64)
                edge_end = np.array([e.tuple[1] for e in G.es], dtype=np.int64)
                n_edges = len(edge_start)

                # Magnitude of the opinion difference across each edge
                opinion_diff = np.array(G.vs[edge_start.tolist()]['opinion']) - np.array(G.vs[edge_end.tolist()]['opinion'])
                opinion_diff = np.abs(opinion_diff)

                #Get the confidence bound for each edge
                try:
                    confidence = results['confidence'][0]

                except:
                    #Load the confidence-bound changes DataFrame
                    if continue_past_bailout:
                        csvfile = (f"{experiment}/delta{delta}-gamma{gamma}/confidence_changes/continue/"
                                   f"delta{delta}-gamma{gamma}--c{c}-mu{mu}-op{opinion_set}.csv")
                        # folder = '/delta' + str(delta) + '-gamma' + str(gamma) + '/'
                        # csvfile = graph_type + str(n) + ('/runtoTmax' * runtoTmax) + folder + 'continue/' + savename + '-op' + str(opinion_set) +'.csv'
                    else:
                        csvfile = (f"{experiment}/delta{delta}-gamma{gamma}/confidence_changes/"
                                   f"delta{delta}-gamma{gamma}--c{c}-mu{mu}-op{opinion_set}.csv")
                        # csvfile = graph_type + str(n) + ('/runtoTmax' * runtoTmax) + '/confidence_changes/delta' + str(delta) + '-gamma' + str(gamma)
                        # csvfile = csvfile + '--c' + str(c) + '-mu' + str(mu) + '-op' + str(opinion_set) + '.csv'
                    confidence_changes_df = pd.read_csv(csvfile)

                    #Calculate the final time
                    t_final = max(confidence_changes_df['t'])

                    confidence = []
                    for i in range(n_edges):
                        node1, node2 = edge_start[i], edge_end[i]
                        filtered_df = confidence_changes_df[confidence_changes_df['node1'] == node1]
                        filtered_df = filtered_df[filtered_df['node2'] == node2]
                        filtered_df = filtered_df.reset_index(drop=True)
                        if len(filtered_df) == 0: 
                            confidence.append(c)
                        #Otherwise, take the most recent confidence value
                        else:
                            confidence.append(filtered_df['confidence'].iloc[-1])

                #Get the edges which have opinion difference < confidence (the confidence bound) and convert them to an edge list
                index = np.argwhere(opinion_diff < confidence)
                index = index.flatten()
                sub_edge_start = edge_start[index]
                sub_edge_end = edge_end[index]
                edge_list = [(sub_edge_start[i], sub_edge_end[i]) for i in range(len(index))]

                #Construct an effective subgraph subG consisting of these edges with possible interactions 
                subG = igraph.Graph()
                subG.add_vertices(n)
                subG.add_edges(edge_list)
                subG.vs['opinion'] = G.vs['opinion']
                subG.es['confidence'] = confidence[index]

                #Assign the initial opinion values
                subG.vs['init_opinion'] = results['init_opinions'][0]

                #Get the opinion clusters, the components of the subgraph
                component_membership = subG.clusters(mode='strong').membership
                n_components = max(component_membership)+1
                components = []
                for i in range(n_components):
                    component = np.nonzero(np.array(component_membership)==i)[0]
                    components.append(component.tolist())

                #Calculate the Shannon entropy for the normalized clusters if n_clusters is not zero from hitting the bailout
                n_clusters = results['n_clusters'][0][0]
                #If we sucessfully determined the clusters, then we calculate the Shannon entropy and max cluster diameter
                if n_clusters > 0:
                    cluster_sizes = [] #get and store the size of each opinion cluster
                    for i in range(n_clusters):
                        key = "cluster" + str(i)
                        cluster = results[key][0]
                        cluster_sizes.append(len(cluster))

                        final_ops = results['final_opinions'][0][cluster]
                    cluster_sizes = np.array(cluster_sizes) / np.sum(np.array(cluster_sizes)) #normalize the cluster sizes
                    #Calculate Shannon's entropy of normalized clusters
                    H = - np.sum(cluster_sizes * np.log(cluster_sizes))

                    #Fraction of edges in the subgraph
                    frac_edges =  len(subG.es) / n_edges

                #If we don't have all the edges in the original graph, plot the effective subgraph:
                if frac_edges < 1:
                    # Create the figure
                    fig, ax = plt.subplots(figsize = (12,10))

                    #Get the norm of the confidence-bound colorbar for edge colors with some rounding
                    vmin = math.floor(10 * min(subG.es['confidence'])) / 10
                    vmax = math.ceil(10 * max(subG.es['confidence'])) / 10
                    if vmax > 1.0: #cap the max confidence at 1
                        vmax = 1.0
                    if vmin < 0.0: #cap the min confidence at 0 
                        vmin = 0.0
                    norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

                    if node_color_type == "initial_opinion":
                        vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in subG.vs['init_opinion']]
                    elif node_color_type == "final_opinion":
                        vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in subG.vs['opinion']]

                    if edge_color_type == "confidence":
                        edge_color = [cmap(norm(confidence)) for confidence in subG.es['confidence']]
                    else:
                        edge_color = "black"

                    visual_style = {
                                    'vertex_size' : 20,
                                    'vertex_frame_width' : 1,
                                    'vertex_frame_color' : 'black',
                                    'vertex_color' : vertex_color,
                                    'edge_width' : 1,
                                    'edge_color' : 'black'
                                    }
                    if network_dataset:
                        visual_style['vertex_size'] = 8

                    # Draw the graph over the plot
                    # Two points to note here:
                    # 1) we add the graph to the axes, not to the figure. This is because
                    #    the axes are always drawn on top of everything in a matplotlib
                    #    figure, and we want the graph to be on top of the axes.
                    # 2) we set the z-order of the graph to infinity to ensure that it is
                    #    drawn above all the curves drawn by the axes object itself.
                    #(`left`, `top`, `width`, `height`)
                    if include_cbar and show_title:
                        graph_artist = GraphArtist(subG, (60, 220, 700, 800), **visual_style)
                    elif include_cbar and not show_title:
                        graph_artist = GraphArtist(subG, (60, 100, 700, 700), **visual_style)
                    else:
                        graph_artist = GraphArtist(subG, (150, 300, 800, 700), **visual_style)
                        # graph_artist = GraphArtist(subG, (150, 300, 800, 700), **visual_style)
                    # graph_artist.set_zorder(float('inf'))
                    ax.artists.append(graph_artist)

                    plt.axis('off') #hide axes

                    if include_cbar:
                        #Colorbar for nodes colored by initial opinion
                        if node_color_type == "initial_opinion":
                            cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=opinion_norm, cmap=opinion_cmap), ax=ax, shrink = 0.9)
                            cbar.ax.tick_params(labelsize=fontsizes['S'])
                            cbar.set_label('Initial opinion', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                        #Add the colorbar for the edges if the confidence bound is changing
                        else:
                            if not (delta == 1.0 and gamma == 0.0):
                                cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, shrink = 0.9)
                                cbar.ax.tick_params(labelsize=fontsizes['S'])
                                cbar.set_label('Confidence bound', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])


                    #Add title
                    if show_title:
                        title = "Adaptive-Confidence DW Model\n"
                        title = title + r"Example Final Effective Graph $\mathit{G_{eff}(T)}$" + "\n" #" for Complete(100) \n"
                        title = (title + r"$\mathit{\gamma}=$" + str(gamma) + r", $\mathit{\delta}=$" + str(delta)
                                   + r", $\mathit{c_0}=$" + str(c) + r", $\mathit{\mu}=$" + str(mu))
                                   # + ', op=' + str(opinion_set))
                        ax.set_title(title, fontsize=fontsizes['L'])
                    else:
                        ax.set_title(" ", fontsize=fontsizes['L'])

                    #Save the file
                    savefile = f"{directory}/delta{delta}-gamma{gamma}--c{c}-mu{mu}-op{opinion_set}"
                    if not show_title:
                        savefile = savefile + "--no_title"
                    savefile = savefile + ".png"
                    # savefile = (directory + '/delta' + str(delta) + '-gamma' + str(gamma) 
                    #            + '--c' + str(c) + '-mu' + str(mu)
                    #            + '-op' + str(opinion_set) + '.png')
                    print(savefile)
                    plt.savefig(savefile, facecolor="white", bbox_inches = "tight")
                    plt.close()


                    #Generate a new plot for each major opinion cluster
                    if n_components > 1:
                        for i in range(n_components):
                            if len(components[i]) > minor_clusters_percent/100 * n: 
                                cluster = subG.induced_subgraph(components[i])

                                #Fraction of edges in the subgraph
                                cluster_original = G.induced_subgraph(components[i])
                                frac_edges =  len(cluster.es) / len(cluster_original.es)

                                #Only plot the components that do not have all their original edges to check them out
                                if frac_edges < 1:

                                    try:
                                        vmin = math.floor(10 * min(cluster.es['confidence'])) / 10
                                        vmax = math.ceil(10 * max(cluster.es['confidence'])) / 10
                                        if vmax > 1.0: #cap the max confidence bound at 1
                                            vmax = 1.0
                                        if vmin < 0.0: #cap the min confidence bound at 0 
                                            vmin = 0.0
                                    except:
                                        vmin = 0.0
                                        vmax = 1.0
                                    norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)

                                    if node_color_type == "initial_opinion":
                                        vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in cluster.vs['init_opinion']]
                                    elif node_color_type == "final_opinion":
                                        vertex_color = [opinion_cmap(opinion_norm(opinion)) for opinion in cluster.vs['opinion']]

                                    if edge_color_type == "confidence":
                                        edge_color = [cmap(norm(confidence)) for confidence in subG.es['confidence']]
                                    else:
                                        edge_color = "black"

                                    visual_style = {
                                            'vertex_size' : 20,
                                            'vertex_frame_width' : 1,
                                            'vertex_frame_color' : 'black',
                                            'vertex_color' : vertex_color,
                                            'edge_width' : 1,
                                            'edge_color' : edge_color,
                                            # 'layout': cluster.layout_lgl() #layout_kamada_kawai() #layout_fruchterman_reingold()
                                            }

                                    # Create the figure
                                    fig, ax = plt.subplots(figsize = (12,10))

                                    # Draw the graph over the plot
                                    # Two points to note here:
                                    # 1) we add the graph to the axes, not to the figure. This is because
                                    #    the axes are always drawn on top of everything in a matplotlib
                                    #    figure, and we want the graph to be on top of the axes.
                                    # 2) we set the z-order of the graph to infinity to ensure that it is
                                    #    drawn above all the curves drawn by the axes object itself.
                                    #(`left`, `top`, `width`, `height`)
                                    if include_cbar and show_title:
                                        graph_artist = GraphArtist(cluster, (60, 200, 700, 800), **visual_style)
                                    elif include_cbar and not show_title:
                                        graph_artist = GraphArtist(cluster, (60, 100, 700, 750), **visual_style)
                                    else:
                                        graph_artist = GraphArtist(cluster, (150, 200, 800, 800), **visual_style)
                                    # graph_artist.set_zorder(float('inf'))
                                    ax.artists.append(graph_artist)

                                    plt.axis('off') #hide axes

                                    if include_cbar:
                                        #Colorbar for nodes colored by initial opinion
                                        if node_color_type == "initial_opinion":
                                            cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=opinion_norm, cmap=opinion_cmap), ax=ax, shrink = 0.9)
                                            cbar.ax.tick_params(labelsize=fontsizes['S'])
                                            cbar.set_label('Initial opinion', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                                        #Add the colorbar for edges if the confidence bound is changing
                                        else:
                                            if not (delta == 1.0 and gamma == 0.0):
                                                cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax, shrink = 0.9)
                                                cbar.ax.tick_params(labelsize=fontsizes['S'])
                                                cbar.set_label('Confidence bound', rotation=270, labelpad=2*fontsizes['M'], fontsize=fontsizes['M'])

                                    #Get the averge opinion and diameter of the cluster
                                    avg_opinion = sum(cluster.vs['opinion']) / len(cluster.vs['opinion'])
                                    diameter = max(cluster.vs['opinion']) - min(cluster.vs['opinion'])

                                    #Add title
                                    if show_title:
                                        # title = "Final Effective Subgraph\n"
                                        title = "Adaptive-confidence DW model\n"
                                        title = title + r"Example final opinion cluster in $\mathit{G_{eff}(T)}$" + "\n" #" for Complete(100) \n"
                                        title = (title + r"$\mathit{\gamma}=$" + str(gamma) + r", $\mathit{\delta}=$" + str(delta)
                                                   + r", $\mathit{c_0}=$" + str(c) + r", $\mathit{\mu}=$" + str(mu))
                                                   # + ', op=' + str(opinion_set))
                                        ax.set_title(title, fontsize=fontsizes['L'])
                                    else:
                                        ax.set_title(" ", fontsize=fontsizes['L'])

                                    #Save the file
                                    savefile = f"{directory}/delta{delta}-gamma{gamma}--c{c}-op{opinion_set}--cluster{i}"
                                    if not show_title:
                                        savefile = savefile + "--no_title"
                                    savefile = savefile + ".png"
                                    print(savefile)
                                    plt.savefig(savefile, facecolor="white", bbox_inches='tight')
                                    plt.close()
