"""
Created on 3/11/2022
Created by Grace Li

This script runs simulations of our adaptive-confidence DW model on the NetScience network data set.

To run this script, at the bottom, modifiy the variables that give the number of nodes, and model parameters 
(confidence-increase parameter gamma, confidence-decrease parameter delta, and initial confidence bound c - this is called c0 in the paper)

For repeatability of the simulations, random seeds for generating sets of initial opinions are stored in opinion_seeds.csv 
and random seeds for the simulations (for selecting random pairs of node for interaction) are stored in sim_seeds.csv
in the folder for each graph type.

For each parameter combination of:
    - confidence-increase parameter gamma
    - confidence-decrease parameter delta
    - initial confidence bound c (called c0 in the paper)
    - opinion set (abbreviated to op in the code), 
the simulation results are stored in a matfile. The script MatfileConsolidator.py is used to calculate the quantities 
we examine and store them in a csv format. After running MatfileConsolidator.py, the various linePlots scripts are used 
to generate plots of our results.

"""

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority
import psutil
psutil.Process().nice(2)# if on *ux

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

# Class for running sets of DW experiments            
class DW_experiment:
    
    #Set class parameters
    tol = 0.02 #Diameter required for convergence critera of opinion clusters
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, G, graph_name="", Tmax = 10**6, return_confidence_changes = False,
                 return_opinion_series = False, opinion_timestep = 1):
        '''
        Initializes class to run dynamic-confidence Deffaunt-Weisbuch simulations for a particular graph dataset
        
        Parameters
        ----------
        G : igraph.graph
            Fixed graph to run the model on. One example is the Karate Club dataset
        graph_name : string
            String to specify the graph name when saving outputs
        Tmax : int, default = 10**6
            Bailout time for the simulations
        return_confidence_changes : bool, default = False
            Specify whether or not to generate and store the csv table of confidence changes over time
        return_opinion_series : bool, default = False
            Specify whether or not to generate and store the opinon trajectories over time
        opinion_timestep : int, default = 1
            If return_opinion_series is True, then this sets how often we get the opinion and add it to the timeseries
        '''
        
        self.G = G
        self.n = G.vcount() #get number of vertices
        
        self.graph_name = graph_name
        self.foldername = graph_name + '_results' #savefolder name for experiment
        
        self.Tmax = Tmax
        self.return_confidence_changes = return_confidence_changes
        self.return_opinion_series = return_opinion_series
        self.opinion_timestep = opinion_timestep
        
        #Check that a directory for this experiment exists, and if not, create it
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            os.makedirs(self.foldername + '/matfiles')
            os.makedirs(self.foldername + '/txtfiles')
        if self.return_confidence_changes:
            if not os.path.exists(self.foldername + "/confidence_changes"):
                os.makedirs(self.foldername + "/confidence_changes")

        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for initial opinions if they don't exist yet
        '''

        self.opinion_seed_file = self.foldername + "/opinion_seeds.csv"
                
        #There is only one opinion seed for a static network, so we generate and save it if it doesn't exist yet
        if not os.path.exists(self.opinion_seed_file):
            df = pd.DataFrame(columns = ['opinion_seed'])
            random.seed(a=None) #reset random by seeding it with the current time
            weight_seed = str(random.randrange(sys.maxsize))
            df.loc[0] = [weight_seed]
            df.to_csv(self.opinion_seed_file, index=False, header=True)
         
            
        #File where the random seeds for simulation are stored
        self.sim_seed_file = self.foldername + '/sim_seeds.csv'
        
        #If sim seed file doesn't already exist, create it
        if not os.path.exists(self.sim_seed_file):
            df = pd.DataFrame(columns = ['c', 'mu', 'delta', 'gamma', 'opinion_set', 'sim_seed'])
            df.to_csv(self.sim_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_DW(self, params):
    
        '''
        Runs DW experiment and saves appropriate output files
        Takes in a dictionary params, containing
        "c" - the initial confidence bound, "mu" - the compromise parameter,
        "delta" - the confidence-decrease parameter, "gamma" - the confidence-increase parameter,
        and "opinion_set" - an integer representing which opinion set to generate from 
        the random opinion seed to run the DW model on. 
        For a complete graph, we only need these parameters.
        For Erdos-Renyi graphs, the 5th parameter, "graph_number" needs to be specified,
        and it represents which randomly generated graph to consider.
        '''
        
        print('Process Number ', getpid())
        print('Params', params)

        #Copy a version of the graph
        G = self.G.copy()
        
        ## Initial set up
        #Unpack parameters
        c, mu = params["c"], params["mu"]
        delta, gamma = params["delta"], params["gamma"]
        opinion_set = params["opinion_set"]

        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #Make sure the appropriate save folders for this delta-gamma combo exist, and if not, create them
        folder = '/delta' + str(delta) + '-gamma' + str(gamma)
        if not os.path.exists(self.foldername + "/matfiles" + folder):
            os.makedirs(self.foldername + '/matfiles' + folder)
        if self.return_confidence_changes:
            if not os.path.exists(self.foldername + "/confidence_changes" + folder):
                os.makedirs(self.foldername + "/confidence_changes" + folder)
                
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        opinion_seed = df['opinion_seed'].values[0]
        opinion_seed = int(opinion_seed)
        
        lock.release()
        
        ## Specify the save file names for matfiles and txtfiles
        savename = ""
        savename = savename + 'delta' + str(delta) + '-gamma' + str(gamma) + '--c' + str(c)
        txtfile = self.foldername + '/txtfiles/' + savename + '.txt'
        savename = savename + '-mu' + str(mu)
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_name, file=f, flush=True)
                print('delta = ', delta, ' and gamma = ', gamma, file=f, flush = True)
                print('c = ', c, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
            
        #Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        ## Read or create a simulation seed for DW node selection for this set of parameters (d, mu, weight_set, opinion_set)
        #Read the random seed csv file as a pandas dataframe
        lock.acquire()
        df = pd.read_csv(self.sim_seed_file)
        
        #Try to get the corresponding dataframe row for this simulation
        row = df[df['c'] == c]
        row = row[row['mu'] == mu]
        row = row[row['delta'] == delta]
        row = row[row['gamma'] == gamma]
        row = row[row['opinion_set'] == opinion_set]
            
        #If there isn't already an entry for this simulation generate and store a simulation seed
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time so we don't keep generating the same sim seeds
            sim_seed = random.randrange(sys.maxsize)
            row = pd.DataFrame(columns = ['c', 'mu', 'delta', 'gamma', 'opinion_set', 'sim_seed'])
            row.loc[0] = [c, mu, delta, gamma, opinion_set, str(sim_seed)]
            df = df.append(row, ignore_index=True)
            df.to_csv(self.sim_seed_file, index=False, header=True)
        else:
            sim_seed = row['sim_seed'].values[0]
            if len(row) > 1:
                with open(txtfile, 'w') as f:
                    print("OH NO! Something went wrong and there are multiple sim_seeds", file=f, flush=True)
                    print(row, file=f, flush=True)
        lock.release()
            
        #Time the DW simulation for this weight + opinion set combo
        start_time = time.time()

        ## Run the DW model using the simulation seed
#         print('Process Number ', getpid(), 'starting DW with sim_seed = ', sim_seed) #deleteline
        outputs = DW.DW(
                            G, c, mu, delta, gamma, random_seed = sim_seed, 
                            tol = self.tol, Tmax = self.Tmax, return_confidence_changes = self.return_confidence_changes,
                            return_opinion_series = self.return_opinion_series, opinion_timestep = self.opinion_timestep
                       )
#         print('Process Number ', getpid(), 'finished DW') #deleteline

        # Dump model outputs into file
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- mu = %f and opinion_set = %s -----" % (mu, opinion_set), file=f, flush=True)

            print("T = %s" % outputs['T'], file=f, flush=True)
            print("Min confidence bound = %.3f, and Max confidence bound = %.3f" % (min(outputs['confidence']), max(outputs['confidence'])), file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'c': c, 'mu': mu, 'delta': delta, 'gamma':gamma, 'opinion_set': opinion_set, 'sim_seed': sim_seed}
        
        #Include the graph-level results
        save_sim['T'] = outputs['T']                    
        save_sim['T_changed'] = outputs['T_changed']
        save_sim['T_acc'] = outputs['T_acc']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']
        
        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()
            
        #Include the node-level results as size n arrays
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change']
        save_sim['n_updates'] = outputs['n_updates']
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        #Edge level information
        save_sim['confidence'] = outputs['confidence']
        
        ## Save the simulation results to a matfile
        folder = '/delta' + str(delta) + '-gamma' + str(gamma) + '/'
        matfile = self.foldername + '/matfiles' + folder + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
        
        ## If return_confidence_changes is true, then save the Pandas DataFrame of confidence bound changes as a csv file
        if self.return_confidence_changes:
            csvfile = self.foldername + '/confidence_changes' + folder + savename + '-op' + str(opinion_set) +'.csv'
            outputs['confidence_df'].to_csv(csvfile, index=False, header=True)
    
        #If we generate the opinion trajectories, then get those too and save to an opinion_trajectories folder
        if self.return_opinion_series:
            save_sim['opinion_series'] = outputs['opinion_series']
            save_sim['opinion_timestep'] = self.opinion_timestep
            del save_sim['init_opinions']
            del save_sim['final_opinions']
            matfile = self.foldername + '/opinion_series/matfiles' + folder + savename + '-op' + str(opinion_set) +'.mat'
            io.savemat(matfile, save_sim)
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    Tmax = 10**7 #Bailout time for ending the simulation
    return_confidence_changes = True #Whether or not to save .csv files of the confidence bound changes over time
    return_opinion_series = False #Whether or not to store and return the opinion trajectories over time in the matfile of simulation results
    opinion_timestep = 100 #Timestep interval to store opinion values
    
    graph_name = 'netscience'
    
    #Read the netscience network and get the largest connected component
    G = igraph.Graph.Read_GML('netscience_data/netscience.gml')
    
    component_membership = G.clusters(mode='strong').membership
    sizes = G.clusters(mode='strong').sizes()
    index_max = sizes.index(max(sizes))
    component = np.nonzero(np.array(component_membership)==index_max)[0]
    
    G = G.induced_subgraph(component)
    print('GCC')
    print('Number of vertices:', len(G.vs))
    print('Number of edges:', len(G.es))
    
    gammas = [0.0] #Confidence-increase parameters
    deltas = [1.0] #Confidence-decrease parameters
    
    cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9] #Initial confidence bound
    mus = [0.1, 0.3, 0.5] #Compromise parameter
    
    opinion_sets = list(range(0,10)) #Which opinion sets to run
    
    ## Generate list of tuples to feed into DW_experiments as parameters
    params_list = []
    for delta in deltas:
        for gamma in gammas:
            for c in cs:
                for mu in mus:
                    for opinion_set in opinion_sets:
                        
                        matfile = (graph_name + '_results/matfiles/'
                                   + 'delta' + str(delta) + '-gamma' + str(gamma) 
                                   + '/delta' + str(delta) + '-gamma' + str(gamma) 
                                   + '--c' + str(c) + '-mu' + str(mu)
                                   + '-op' + str(opinion_set) + '.mat')
                        
                        try:
                            results = io.loadmat(matfile)
                            
                        except:
                            print(matfile)
                            param_dict = {"delta": delta, "gamma": gamma,
                                          "c": c, "mu": mu, 
                                          "opinion_set": opinion_set}
                            params_list.append(param_dict) 
    
    #Initialize experiment class
    experiment = DW_experiment(G, graph_name = graph_name, Tmax = Tmax, return_confidence_changes = return_confidence_changes,
                               return_opinion_series = return_opinion_series, opinion_timestep = opinion_timestep)
    experiment.generate_seed_files()

    l = multiprocessing.Lock()

    with multiprocessing.Pool(processes=35, initializer=init, initargs=(l,)) as pool:
        pool.map(experiment.run_DW, params_list)