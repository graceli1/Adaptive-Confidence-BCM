'''
Author: Grace Li
Date: 3/1/2022

This script generates heatmaps of the results of the simulations for our adaptive-confidence DW BCM on complete graphs.
We do not use these heatmaps in the paper and switch to line plots instead for clearer visualizations.

For each quantity of interest, this script generates a single figure. Each figure has a heatmap for each graph size 
(n = number of nodes). The horizontal axis of each heatmap has the compromise parameter mu, and the vertical axis of 
each heatmap has the initial confidence bound c0 (called c in the code). We generate each point square on the heatmap from 
10 numerical simulations with different sets of initial opinions drawn uniformly at random.

'''

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own adaptive-confidence BCM module
import sys
sys.path.append('..') #look one directory above
sys.path.append('../..') #look two directories above
import AdaptiveConfidenceBCM

#name of experiment folder
graph_type = "complete"
all_n = True

decimal_places = 2
show_labels = True
print_error = True
#se for standard error, sd for standard deviation
error_type = "sd" 


runtoTmax = False
toltest = False
tol = 0.01 #specify the tolerance value if its not the standard 0.02

#Specify which quantities to plot
keys = [
        'log(T)', 'entropy',
        'n_major', 'n_minor',
        'wt_avg_frac_edges_cluster'
       ]

#Graph sizes N
ns = [100, 200]
if all_n:
    ns = [25, 50, 100, 200]

#delta-gamma combos (the rows of the heatmap grid)
#delta is the confidence-decrease parameter, and gamma is the confidence-increase parameter
delta_gamma = [(1.0, 0.0), (0.5, 0.1)]

#Initial confidence bound
cs = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
cs = np.flip(cs) #flip cs so that heatmaps are ordered properly

#Compromise parameter
mus = [0.1, 0.3, 0.5]

#Plot font size parameters
fontsizes = {'XS': 18, 'S': 25, 'M': 25, 'L': 45, 'XL': 50, 'XXL': 60}

# cmap = "rainbow"
nodes = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 0.9, 1.0]
colors = ['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58', '#002652'] #Yellow green blue
# colors = ['#fff7f3','#fde0dd','#fcc5c0','#fa9fb5','#f768a1','#dd3497','#ae017e','#7a0177','#49006a', '#0e0014'] #Red purple
base_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip the colors
reverse_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))


#These are the keys that require plots printed in exponential notation and a wider figure size
#figsize = (fig_width * len(distributions), sum(subfig_heights)) #width x height
base_fig_width = 6
if print_error and error_type == "se":
    base_fig_width = 7
wider_figure_keys = ['avg_local_receptiveness', 'avg_opinion_diff', 'op_var']
wider_fig_width = base_fig_width + 2 

# We want to consider the graph-level results of various quantities
# Translate the quantity into text for the savefile and plot titles using these dictionaries
plot_name = {
                'log(T)': 'T',
                'n_clusters': 'clusters', 
                'n_minor': 'n_minor',
                'n_major': 'n_major',
                'entropy' : 'entropy',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis',
                'frac_edges': 'frac_edges', 
                'wt_avg_frac_edges_cluster': 'wt_avg_frac_edges',
                'deleted_within_of_total': 'deleted_within_of_total',
                'deleted_within_of_deleted': 'deleted_within_of_deleted'
            }
plot_title = {
                'log(T)': r"$\log_{10}(T_f)$", 
                'n_clusters': 'Number of clusters',
                'n_minor': 'Number of minor clusters',
                'n_major': 'Number of major clusters',
                'entropy': 'Shannon entropy ' + r"$H(T_f)$",
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis',
                'frac_edges': 'Fraction of edges', 
                'wt_avg_frac_edges_cluster': r"$W(T_f)$",
                'deleted_within_of_total': 'Fraction of\ndisconnected edges',
                'deleted_within_of_deleted': 'Fraction of within-cluster\ndisconnected edges'
             }

# Plot heat maps of all simulation values

#Check that a directory exists, and if not, create it
directory = 'graph_size_plots/'
if all_n:
    directory = directory + "all_ns/"
if not os.path.exists(directory):
    os.makedirs(directory)

stats = ['mean', 'sd', 'se']
save_suffix = {'mean':'', 'sd':'_sd', 'se': '_se'}
title_prefix = {'mean':'', 'sd': 'SD of ', 'se': 'SE of '}

# ----------------------------------------------------
#Loop through the quantities of interest
for key in keys:
    
    print('Generating plot of ' + key)
    
    #Pick the figure size parameters based on the key
    fig_width = base_fig_width
    if key in wider_figure_keys:
        fig_width = wider_fig_width
    figsize = (fig_width * len(ns), 8 * len(delta_gamma))
    
    #Pick the colormap based on if we want higher or lower numbers more important
    if key in ['avg_local_receptiveness']: #lower numbers are more important
        cmap = reverse_cmap
    else:
        cmap = base_cmap

    heatmaps = {} #dictionary to store the heatmaps with axis c and mu for each n-delta-gamma combo
    
    for pair in range(len(delta_gamma)): #each delta-gamma combo is a row in our heatmap grid
        delta = delta_gamma[pair][0]
        gamma = delta_gamma[pair][1]
        
        for n in ns: #for each graph size

            #Load the simulation results stored csv files (from MatfileConsolidator.ipynb)
            filename = experiment = graph_type + str(n) + ("/runtoTmax" * runtoTmax) #savefolder name for experiment
            filename = filename + '/combined_results/delta' + str(delta) + '-gamma' + (str(gamma) + ('--tol' + str(tol)) * toltest) + '.csv'
            sim_results = pd.read_csv(filename)

            #Make sure the opinion_set, and time steps are integers instead of floats
            sim_results = sim_results.astype({'opinion_set': int, 'T': int, 'T_changed': int})

            #Calculate the log of time steps to make it easier to visualize
            sim_results['log(T)'] = np.log10(sim_results['T'])
            sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

            #Create zero matricies for each heatmap we want to generate
            mean, sd, se = np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus))), np.zeros((len(cs), len(mus)))

            #Fill in the heatmap values 
            # rows correspond to d
            for row in range(0,len(cs)):
                c = cs[row]

                # columns correspond to mu
                for col in range(0, len(mus)):
                    mu = mus[col]

                    #filter the data table by the d and mu values
                    df = sim_results[sim_results['c'] == c]
                    df = df[df['mu'] == mu]

                    #Calculate the descriptive stats
                    stats = df[key].describe()

                    #Add the stats values to the appropriate matrix (heatmap)
                    mean[row][col] = stats['mean']
                    sd[row][col] = stats['std']
                    # se[row][col] = stats.sem(array)
                    se[row][col] = stats['std'] / np.sqrt(len(df[key]))

            #store the heatmaps
            heatmap_key = 'n' + str(n) + '--pair' + str(pair)
            heatmaps[heatmap_key] = {'mean':mean, 'sd':sd, 'se':se}
            
    # ----------------------------------------------------
    #Plot the heatmaps
    for stat in ['mean']:
        
        #plot_maps = [heatmaps[heatmap_key][stat] for heatmap_key in heatmap_keys] #collect all the heatmaps of the same quantity - i.e. average, sd
        plot_maps = {heatmap_key: heatmaps[heatmap_key][stat] for heatmap_key in heatmaps.keys()}  #collect all the heatmaps of the same quantity - i.e. average, sd
        fig, axs = plt.subplots(
                                nrows = len(delta_gamma), ncols = 1, sharey = True, sharex = True,
                                constrained_layout = True,
                                figsize = figsize
                               ) #create plot
        # remove the axes for the big supfigures
        for ax in axs:
            ax.remove()
        
        # add subfigure (for our row of heatmaps) per subplot
        gridspec = axs[0].get_subplotspec().get_gridspec()
        subfigs = [fig.add_subfigure(gs) for gs in gridspec]
        
        #cbar_ax = fig.add_axes([1.01, .1, .02, .8]) #add colorbar - left, bottom, width, height
        cbar_ax = fig.add_axes([.05, -0.08, .9, .05]) 
    
        #Get the maximum value for the colorbar
        vmax = [np.nanmax(plot_maps[heatmap_key]) for heatmap_key in plot_maps.keys()]
        vmax = AdaptiveConfidenceBCM.round_decimals_up(max(vmax), 1)
    
        #Get the minimum value for the color bar
        vmins = []
        for heatmap_key in plot_maps.keys():
            matrix = plot_maps[heatmap_key]
            # If we have -inf values, i.e. if a value is ln(0), then set to 0 so it will plot
            if (np.amin(matrix) == - np.inf):
                matrix[matrix == - np.inf] = 0
                plot_maps[heatmap_key] = matrix
                vmins.append(0)
            else:
                vmins.append(AdaptiveConfidenceBCM.round_decimals_down(np.nanmin(matrix), 1))
        vmin = min(vmins)
    
        # ----------------------------------------------------
        #PLot the heatmap row for each delta--gamma combination
        for i, subfig in enumerate(subfigs): #i is the row of the heatmap grid
                
            delta, gamma = delta_gamma[i][0], delta_gamma[i][1]    
            subfig.suptitle(r"$\delta$ = " + str(delta) + r", $\gamma$ = " + str(gamma), 
                            fontsize = fontsizes['XXL'], color='darkblue') #, fontweight='semibold')
                
            # create 1 x len(ns) subplots per subfig
            axs = subfig.subplots(nrows=1, ncols=len(ns), sharey=True)
            
            for j in range(len(ns)):
                
                #Get the heatmap key
                n = ns[j]
                heatmap_key = 'n' + str(n) + '--pair' + str(i)

                ax = axs[j] #single out the right axis

                matrix = plot_maps[heatmap_key] #get the matrix for the heatmap

                #Convert matrix to dataframe for axis labels
                df = pd.DataFrame(matrix, index = cs, columns = mus)

                #Plot heat map with or without entry values depending on show_values
                if show_labels:
                    #Get the annotated values of average +/- sd
                    if print_error and stat == 'mean':
                        mean = np.array(df)
                        if error_type == 'sd':
                            error = heatmaps[heatmap_key]['sd']
                        elif error_type == 'se':
                            error = heatmaps[heatmap_key]['se']
                        labels = AdaptiveConfidenceBCM.print_avg_with_std(mean, error)

                        #Plot heat map with labels of average +/- sd
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, 
                                    annot = labels, fmt = "", annot_kws={"size":fontsizes['XS']})

                    else:
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = True,
                                    fmt="0."+str(decimal_places)+"f", annot_kws={"size":fontsizes['XS']})
                else:
                    g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = False)
                #ax = sns.heatmap(df, cmap = "rainbow", vmin = vmin, vmax = vmax)

                # #Set subplot axis labels
                # if j == 0:
                #     ax.set_ylabel(r"Initial Confidence Bound ($c_0$)", fontsize = fontsizes['XL'])
                # if i == len(delta_gamma) - 1:
                #     ax.set_xlabel(r"Learning Rate ($\mu$)", fontsize = fontsizes['XL'])

                #Set subplot title
                ax.set_title("N = " + str(n), fontsize = fontsizes['L'])

                #Set tick label size
                ax.set_xticklabels(mus, size = fontsizes['S'])
                ax.set_yticklabels(cs, rotation = 0, size = fontsizes['S'])
    
        # ----------------------------------------------------
        #Set colorbar and tick label font size
        cbar_ax.tick_params(labelsize=fontsizes['M'])
        cbar_ax.set_xlabel(title_prefix[stat] + plot_title[key], fontsize=fontsizes['XL'])
        #cbar_ax.tick_params(axis='both', labelsize=fontsizes['M'])
        
        #Large shared axis labels
        fig.supylabel("Initial confidence bound ($c_0$)", fontsize = fontsizes['XL'])
        fig.supxlabel("Compromise parameter ($\mu$)", fontsize = fontsizes['XL'])
        
        #Save the plot
        savefile = directory + plot_name[key] + save_suffix[stat]
        savefile = savefile + ('--labeled' * show_labels)
        if print_error and stat == 'mean':
            savefile = f"{savefile}--with_{error_type}"
        savefile = savefile + ".png"
        plt.savefig(savefile, bbox_inches='tight', facecolor='white')
        plt.show()
        