'''
Author: Grace Li
Date: 3/29/2022

This script continues simulations of our adaptive-confidence DW model on complete graphs. If after running 
completeDW.py, simulations reached bailout time (but their matfiles were saved), the simulations can be continued
where they left off by entering the model parameters here. This script will read the stored matfile and continue the
simulation with a longer bailout time set. The resulting continued simulation is stored in a separate "continue" matfile.

'''

import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority
import psutil
psutil.Process().nice(12)# if on *ux

# Import our own adaptive-confidence BCM module
import sys
sys.path.append('..') #look one directory above
sys.path.append('../..') #look two directories above
import AdaptiveConfidenceBCM

# Class for running sets of DW experiments            
class DW_experiment:
    
    #Set class parameters
    tol = 0.02 #Diameter required for convergence critera of opinion clusters
    return_confidence_changes = True #Whether or not to save .csv files of the confidence changes over time
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, graph_type, n, Tmax, p=False):
        '''
        Initializes class to run Deffaunt-Weisbuch simulations for a particular graph type
        
        Parameters
        ----------
        graph_type : string
            String specifying the graph type. Currently the options are "complete" and "erdos-renyi"
        n : int
            Number of nodes in the graph(s) considered
        p : float, required only if graph_type == "erdos-renyi"
            If the graph_type is "erdos-renyi", then p is a required parameter. p is the edge probability
            in the G(n,p) Erdos-Renyi model.
        '''
        
        self.graph_type = graph_type
        self.n = n
        
        self.Tmax = Tmax
        
        self.foldername = graph_type + str(n) #savefolder name for experiment
        
        if self.graph_type == "erdos-renyi":
            self.p = p

        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for random graphs (if not complete), and initial opinions if they don't exist yet
        '''
        
        self.graph_seed_file = self.graph_type + str(self.n) + "/graph_seeds.csv"
        self.opinion_seed_file = self.graph_type + str(self.n) + "/opinion_seeds.csv"

        if self.graph_type == "complete":
                
            #There is only one opinion seed for a complete graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['opinion_seed'])
                random.seed(a=None) #reset random by seeding it with the current time
                weight_seed = str(random.randrange(sys.maxsize))
                df.loc[0] = [weight_seed]
                df.to_csv(self.opinion_seed_file, index=False, header=True)
         
        elif self.graph_type == "erdos-renyi":
            
            #There is only graph seed per p value for an erdos-renyi graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.graph_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph_seed'])
                df.to_csv(self.graph_seed_file, index=False, header=True)
            df = pd.read_csv(self.graph_seed_file)
            row = df[df['p'] == self.p]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                graph_seed = str(random.randrange(sys.maxsize))
                row = pd.DataFrame(columns = ['p', 'graph_seed'])
                row.loc[0] = [self.p, graph_seed]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.graph_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            
        #File where the random seeds for simulation are stored
        if self.graph_type == 'complete':
            self.sim_seed_file = self.foldername + '/sim_seeds.csv'
        if self.graph_type == 'erdos-renyi':
            self.sim_seed_file = self.foldername + '/sim_seeds/p-' + str(self.p) + '.csv'
        
        #If sim seed file doesn't already exist, create it
        if not os.path.exists(self.sim_seed_file):
            df = pd.DataFrame(columns = ['c', 'mu', 'delta', 'gamma', 'opinion_set', 'sim_seed'])
            if self.graph_type == "erdos-renyi":
                df.insert(0,'graph_number','')
                df.insert(0,'p','')
            df.to_csv(self.sim_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_DW(self, params):
    
        '''
        Runs DW experiment and saves appropriate output files
        Takes in a dictionary params, containing
        "c" - the initial confidence bound, "mu" - the compromise parameter,
        "delta" - the confidence-decrease parameter, "gamma" - the confidence-increase parameter
        and "opinion_set" - an integer representing which opinion set to generate from 
        the random opinion seed to run the DW model on. 
        For a complete graph, we only need these parameters.
        For Erdos-Renyi graphs, the 5th parameter, "graph_number" needs to be specified,
        and it represents which randomly generated graph to consider.
        '''
        
        print('Process Number ', getpid())
        print('Continuing matfile', params["previous_matfile"])

        ## Initial set up
        #Unpack parameters
        previous_matfile = params["previous_matfile"]
        c, mu = params["c"], params["mu"]
        delta, gamma = params["delta"], params["gamma"]
        opinion_set = params["opinion_set"]
        if self.graph_type == 'erdos-renyi':
            graph_number = params["graph_number"]

        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #Make sure the appropriate save folders for this delta-gamma combo exist, and if not, create them
        #To make sure we don't overwrite the original files and mess up, make a separate continue folder to save to
        folder = '/delta' + str(delta) + '-gamma' + str(gamma)
        if not os.path.exists(self.foldername + "/matfiles" + folder + '/continue'):
            os.makedirs(self.foldername + '/matfiles' + folder + '/continue')
            os.makedirs(self.foldername + '/txtfiles/continue')
        if self.return_confidence_changes:
            if not os.path.exists(self.foldername + "/confidence_changes" + folder + '/continue'):
                os.makedirs(self.foldername + "/confidence_changes" + folder + '/continue')
        
        # Get the random graph seed if not a complete graph
        if self.graph_type == 'erdos-renyi':
            df = pd.read_csv(self.graph_seed_file)
            row = df[df['p'] == self.p]
            graph_seed = row['graph_seed'].values[0]
            graph_seed = int(graph_seed)
                
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        if self.graph_type == 'complete':
            opinion_seed = df['opinion_seed'].values[0]
            opinion_seed = int(opinion_seed)
        elif self.graph_type == 'erdos-renyi':
            row = df[df['p'] == self.p]
            row = row[row['graph'] == graph_number]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                opinion_seed = random.randrange(sys.maxsize)
                row = pd.DataFrame(columns = ['p', 'graph', 'opinion_seed'])
                row.loc[0] = [self.p, graph_number, str(opinion_seed)]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            else:
                opinion_seed = row['opinion_seed'].values[0]
                opinion_seed = int(opinion_seed)
        lock.release()
        
        ## Specify the save file names for matfiles and txtfiles
        savename = ""
        if self.graph_type == 'erdos-renyi':
            savename = 'p' + str(self.p) + '/graph' + str(graph_number) + '/'
            savename = savename + 'p' + str(self.p) + '-graph' + str(graph_number) + "--"
        savename = savename + 'delta' + str(delta) + '-gamma' + str(gamma) + '--c' + str(c)
        txtfile = self.foldername + '/txtfiles/continue/' + savename + '.txt'
        savename = savename + '-mu' + str(mu)
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_type, ", n =", self.n, file=f, flush=True)
                if self.graph_type == 'erdos-renyi':
                    print("p =", self.p, ", graph_number = ", graph_number, file=f, flush=True)
                    print('graph_seed = ', graph_seed, file=f, flush=True)
                print('delta = ', delta, ' and gamma = ', gamma, file=f, flush = True)
                print('c = ', c, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
        
        # Generate graph
        if self.graph_type == "complete":
            G = igraph.Graph.Full(self.n)
        elif self.graph_type == "erdos-renyi":
            #Reinitialize the random seed and generate the corresponding graph number from that seed
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G = igraph.Graph.Erdos_Renyi(self.n, self.p)
            
        #Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        ## Read or create a simulation seed for DW node selection for this set of parameters (d, mu, weight_set, opinion_set)
        #Read the random seed csv file as a pandas dataframe
        lock.acquire()
        df = pd.read_csv(self.sim_seed_file)
        
        #Try to get the corresponding dataframe row for this simulation
        row = df[df['c'] == c]
        row = row[row['mu'] == mu]
        row = row[row['delta'] == delta]
        row = row[row['gamma'] == gamma]
        row = row[row['opinion_set'] == opinion_set]
        if self.graph_type == "erdos_renyi":
            row = row[row['p'] == self.p]
            row = row[row['graph_number'] == graph_number]
            
        #If there isn't already an entry for this simulation generate and store a simulation seed
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time so we don't keep generating the same sim seeds
            sim_seed = random.randrange(sys.maxsize)
            row = pd.DataFrame(columns = ['c', 'mu', 'delta', 'gamma', 'opinion_set', 'sim_seed'])
            row.loc[0] = [c, mu, delta, gamma, opinion_set, str(sim_seed)]
            if self.graph_type == "erdos-renyi":
                row.insert(0,'graph_number', graph_number)
                row.insert(0,'p', self.p)
            df = df.append(row, ignore_index=True)
            df.to_csv(self.sim_seed_file, index=False, header=True)
        else:
            sim_seed = row['sim_seed'].values[0]
            if len(row) > 1:
                with open(txtfile, 'w') as f:
                    print("OH NO! Something went wrong and there are multiple sim_seeds", file=f, flush=True)
                    print(row, file=f, flush=True)
        lock.release()
            
        #Read the results of the previous simulation
        previous_results = io.loadmat(previous_matfile)
        G.vs['opinion'] = previous_results['final_opinions'][0] #Continue from the opinions where we left off
            
        #Time the DW simulation for this weight + opinion set combo
        start_time = time.time()

        ## Run the DW model using the simulation seed
        # Continue the simulation using the previous end time and end confidence values
        # print('Process Number ', getpid(), 'starting DW with sim_seed = ', sim_seed) #deleteline
        outputs = AdaptiveConfidenceBCM.DW(G, c, mu, delta, gamma, random_seed = sim_seed, 
                        t_start = previous_results['T'][0][0], confidence_start = previous_results['confidence'][0],
                        tol = self.tol, Tmax = self.Tmax, return_confidence_changes = self.return_confidence_changes)
        # print('Process Number ', getpid(), 'finished DW') #deleteline

        # Dump model outputs into text file
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- mu = %f and opinion_set = %s -----" % (mu, opinion_set), file=f, flush=True)

            print("T = %s" % outputs['T'], file=f, flush=True)
            print("t_start = %s" % previous_results['T'][0][0], file=f, flush=True)
            print("Min confidence = %.3f, and Max confidence = %.3f" % (min(outputs['confidence']), max(outputs['confidence'])), file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'c': c, 'mu': mu, 'delta': delta, 'gamma':gamma, 'opinion_set': opinion_set, 'sim_seed': sim_seed}
        
        #Include the graph-level results
        save_sim['T'] = outputs['T']                    
        save_sim['T_changed'] = outputs['T_changed'] + previous_results['T_changed'][0][0]
        save_sim['T_acc'] = outputs['T_acc']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']
        
        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()
            
        #Include the node-level results as size n arrays
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change'] + previous_results['total_change'][0]
        save_sim['n_updates'] = outputs['n_updates'] + previous_results['n_updates'][0][0]
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        #Edge level information
        save_sim['confidence'] = outputs['confidence']
        
        ## Save the simulation results to a matfile
        folder = '/delta' + str(delta) + '-gamma' + str(gamma) + '/'
        matfile = self.foldername + '/matfiles' + folder + 'continue/' + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
        
        ## If return_confidence_changes is true, then save the Pandas DataFrame of confidence change as a csv file
        if self.return_confidence_changes:
            csvfile = self.foldername + '/confidence_changes' + folder + 'continue/' + savename + '-op' + str(opinion_set) +'.csv'
            outputs['confidence_df'].to_csv(csvfile, index=False, header=True)
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    n = 200 #Complete graph size
    
    #Tmax = 5 * 10**6 #Bailout time for ending the simulation for delta = 0.5, gamma = 0.1 runs
    Tmax = 5 * 10**6 #Bailout time for ending the simulation
    
    gammas = [0.1] #Confidence-increase parameters
    deltas = [0.5] #Confidence-decrease parameters
    
    cs = [0.1] #Initial confidence bound
    mus = [0.3, 0.5] #Compromise parameter
    
    opinion_sets = list(range(0,10)) #Which opinion sets to run
    
    ## Generate list of tuples to feed into DW_experiments as parameters
    params_list = []
    for delta in deltas:
        for gamma in gammas:
            for c in cs:
                for mu in mus:
                    for opinion_set in opinion_sets:
                        
                        #See if we've continued this simulation before
                        try:
                            matfile = ('complete' + str(n) + '/matfiles/delta' 
                                       + str(delta) + '-gamma' + str(gamma) + '/continue/'
                                       + 'delta' + str(delta) + '-gamma' + str(gamma) 
                                       + '--c' + str(c) + '-mu' + str(mu)
                                       + '-op' + str(opinion_set) + '.mat')
                            results = io.loadmat(matfile)
                        
                        except:
                            matfile = ('complete' + str(n) + '/matfiles/'
                                       + 'delta' + str(delta) + '-gamma' + str(gamma)
                                       + '/delta' + str(delta) + '-gamma' + str(gamma)
                                       + '--c' + str(c) + '-mu' + str(mu)
                                       + '-op' + str(opinion_set) + '.mat')

                            try:
                                results = io.loadmat(matfile)

                                if results['bailout'][0][0]:
                                    param_dict = {"previous_matfile": matfile,
                                                  "delta": delta, "gamma": gamma,
                                                  "c": c, "mu": mu, 
                                                  "opinion_set": opinion_set}
                                    params_list.append(param_dict) 

                            except:
                                with open('complete' + str(n) + '/filenotfound.txt', 'a') as f:
                                    print('File not found:', matfile, file=f, flush=True)
                                
    #Initialize experiment class
    experiment = DW_experiment('complete', n = n, Tmax = Tmax)
    experiment.generate_seed_files()

    l = multiprocessing.Lock()

    with multiprocessing.Pool(processes=5, initializer=init, initargs=(l,)) as pool:
        pool.map(experiment.run_DW, params_list)